﻿using UnityEngine;
using System.Collections;

public class ParallaxEffect : MonoBehaviour
{
	void Update()
    {
        Quaternion rotation = this.transform.rotation;
        Quaternion cameraRotation = Camera.main.transform.rotation;
        this.transform.rotation = Quaternion.Lerp(rotation, cameraRotation, 0.5f);
    }
}
