﻿using UnityEngine;
using System.Collections;

public static class AudioManager
{
    private const string musicKeyPrefs = "MusicOn";
    private const string sfxKeyPrefs = "SfxOn";

    public static bool MusicOn { get; private set; }
    public static bool SfxOn { get; private set; }

    private static AudioSource audioSource;
    private static AudioClip cachedAudioClip;

    private static bool initialized = false;

    public static void Init()
    {
        if (!initialized)
            initialized = true;
        else
            return;

        audioSource = ((GameObject)GameObject.FindGameObjectWithTag("Audio")).GetComponent<AudioSource>();
        cachedAudioClip = audioSource.clip;

        var userMusic = PlayerPrefs.GetInt(musicKeyPrefs, 1);
        var userSFX = PlayerPrefs.GetInt(sfxKeyPrefs, 1);
        SwitchMusic(userMusic == 1);
        SwitchSFX(userSFX== 1);
    }

    public static void Play()
    {
        Stop();
        if (MusicOn && cachedAudioClip != null)
        {
            audioSource.clip = cachedAudioClip;
            audioSource.Play();
        }
    }

    public static void Stop()
    {
        audioSource.Stop();
    }

    public static void SetAudio(AudioClip clip)
    {
        cachedAudioClip = clip;
        if (MusicOn)
            Play();
    }

    public static void SwitchMusic(bool enabled)
    {
        Debug.Log("Music " + enabled);
        MusicOn = enabled;

        if (enabled)
        {
            Play();
        }
        else
        {
            Stop();
        }
    }

    public static void SwitchSFX(bool enabled)
    {
        Debug.Log("SFX " + enabled);
        SfxOn = enabled;
    }
}