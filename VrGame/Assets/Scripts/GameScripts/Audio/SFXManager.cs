﻿using UnityEngine;
using System.Collections;

public static class SFXManager
{
    [SerializeField]
    static AudioClip killSound;

    [SerializeField]
    static AudioClip clickSound;

    [SerializeField]
    static AudioClip walkSound;

    [SerializeField]
    static AudioClip winSound;

    [SerializeField]
    static AudioClip loseSound;
    
    [SerializeField]
    static AudioClip selectItemSound;
    
    static bool sfxOn;
    static bool initialized = false;

    static void Init()
    {
        if (initialized)
            return;
        else
            initialized = true;

        sfxOn = AudioManager.SfxOn;
    }
    
    public static void PlaySFX(AudioClip clip)
    {
        if (sfxOn)
        {
            AudioManager.SetAudio(clip);
        }
    }
}