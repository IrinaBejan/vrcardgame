﻿using UnityEngine;
using System.Collections;

public class InputTarget : MonoBehaviour
{
    public const string inputKey = "inputKey";

    private GameObject target;

    public GameObject Target
    {
        get
        {
            return target;
        }
        set
        {
            target = value;
        }
    }
}
