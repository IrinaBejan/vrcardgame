﻿using UnityEngine;

public static class GameObjectExtension
{
    private const string missingFieldWarning = "Seeking {0} on {1}. TO INITIALIZE THIS!";
    public static T GetInterface<T>(this GameObject gameObject) where T : class
    {
        return gameObject.GetComponent(typeof(T)) as T;
    }

    public static T[] GetInterfacesInChildren<T>(this GameObject gameObject) where T : class
    {
        return gameObject.GetComponentsInChildren(typeof(T)) as T[];
    }

    public static void CheckAndInitializeWithInterface<T>(this GameObject gameObject, ref T field, bool warn = false) where T : class
    {
        UnityEngine.Object.Equals(null, null);
        //Unity.Object.Equals() override
        if (field == null || field.Equals(null))
        {

#if DEBUG
            if (warn)
            {
                Debug.LogWarning(string.Format(missingFieldWarning, typeof(T).Name, gameObject.name));
            }
#endif

            field = gameObject.GetInterface<T>();
        }

        if (!warn && field == null)
        {
            Debug.LogError(string.Format("Interface missing for component '{0}' on object '{1}'.", typeof(T).Name, gameObject.name));
        }
    }

    public static T GetInterface<T>(this Component component) where T : class
    {
        return component.GetComponent(typeof(T)) as T;
    }
}
