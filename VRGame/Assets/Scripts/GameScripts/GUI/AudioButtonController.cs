﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class AudioButtonController : CircularButtonController
{
    private float normalXScale = 1f;
    public override void Awake()
    {
        base.Awake();

        normalXScale = progressMeter.transform.localScale.x;
    }

    public bool IsFlipped
    {
        get { return progressMeter.transform.localScale.x == -normalXScale; }
    }

    public override void Update()
    {
        if (!SceneController.Instance.IsPlayingSound)
        {
            if (IsFlipped)
            {
                Flip();
            }

            base.Update();
        }
        else
        {
            progressMeter.fillAmount = 1f - SceneController.Instance.SoundClipProgress;
        }
    }

    public override void Action()
    {
        SceneController.Instance.PlaySound();

        Flip();
    }

    public void Flip()
    {
        progressMeter.transform.localScale = new Vector3(-progressMeter.transform.localScale.x, progressMeter.transform.localScale.y, progressMeter.transform.localScale.z);

        progressMeter.fillAmount = 0f;
    }
}
