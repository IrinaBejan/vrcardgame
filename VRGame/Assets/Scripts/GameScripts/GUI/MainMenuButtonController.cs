﻿using UnityEngine;
using System.Collections;

public class MainMenuButtonController : GraphicButtonController
{
    public override void Awake()
    {
        base.Awake();
    }

    public override void Update()
    {
        base.Update();
    }

    public override void Action()
    {
        base.buttonComponent.onClick.Invoke();
    }

}
