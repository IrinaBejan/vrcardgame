﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameUIController : MonoBehaviour
{
    public Image[] manaUser;
    public Image[] manaOpponent;

    public GameObject cardPanel;
    public GameObject manaPanel;

    public Text opponentLife;
    public Text userLife;

    readonly public Color enabledColor = new Color(255f, 255f, 255f);
    readonly public Color disabledColor = new Color(211f,211f,211f);

    public void Start()
    {
        manaPanel.SetActive(true);
    }
    public void SetLifeOpponent(int life)
    {
        SetLife(opponentLife, life);
    }

    public void SetLifeUser(int life)
    {
        SetLife(userLife, life);
    }

    public void SetManaUser(int mana)
    {
        SetMana(manaUser,mana);
    }
    public void SetManaOpponent(int mana)
    {
        SetMana(manaOpponent, mana);
    }

    public void SetStats(int life, int mana)
    {
        SetLife(opponentLife,life);
        SetLife(userLife,life);
        SetMana(manaUser, mana);
        SetMana(manaOpponent, mana);
    }

    public void SetLife(Text player, int life)
    {
        player.text = life.ToString();
    }

    public void SetMana(Image[] manaPlayer, int mana)
    {
        var availableMana = manaPlayer.Length;
        for (int i = 0; i < mana; i++)
            manaPlayer[i].color = enabledColor;
        for (int i = mana; i < availableMana; i++)
            manaPlayer[i].color = disabledColor;
    }
}

