﻿using SprocketTools.Base;
using UnityEngine;
using UnityEngine.UI;
using System;

public abstract class GraphicButtonController : KMonoBehaviour
{
    public float activationTime;

    public Collider colliderComponent;

    public Button buttonComponent;

    public Guid guid;

    public abstract void Action();

    public virtual void Awake()
    {
        guid = Guid.NewGuid();

        if (colliderComponent == null)
            colliderComponent = GetComponentInChildren<Collider>();

        if (buttonComponent == null)
           buttonComponent = GetComponentInChildren<Button>();
    }

    public virtual void Update()
    {
        if (buttonComponent == null)
           return;
        if (SceneManager.TouchModeEnabled)
        {
#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit touchHit;
                Ray touchRay = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(touchRay, out touchHit, 500))
                    if((touchHit.collider == colliderComponent))
                {
                    Debug.Log("Distance" + touchHit.distance.ToString());
                    Action();
                }
            }
#else
			if (Input.touchCount == 1)
			{  
				if (Input.GetTouch(0).phase == TouchPhase.Began)
				{
					RaycastHit touchHit;
					Ray touchRay = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                  
					if (Physics.Raycast(touchRay, out touchHit, 500) && (touchHit.collider == colliderComponent))
					{
						Action();
					}
				}
			}
#endif
        }
        else
        {
            RaycastHit hit;
            Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
            
            if (Physics.Raycast(ray, out hit, 500) && (hit.collider == colliderComponent))
            {
             
                SceneManager.FillEyePlus(Time.deltaTime,activationTime);

                if (SceneManager.fillAmountTimer > activationTime)
                {
                    Action();
                }

                SceneManager.lastGuid = guid;
            }
            else
            {
                SceneManager.FillEyeMinus(Time.deltaTime, activationTime);
            }
        }
    }
}
