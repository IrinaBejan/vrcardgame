﻿using SprocketTools.Base;
using UnityEngine;

[System.Serializable]
public partial class SkyboxUnwrapTextures
{
    //public KResourceReference frontResource = new KResourceReference(typeof(Texture));
    //public KResourceReference leftResource = new KResourceReference(typeof(Texture));
    //public KResourceReference rightResource = new KResourceReference(typeof(Texture));
    //public KResourceReference backResource = new KResourceReference(typeof(Texture));
    //public KResourceReference upResource = new KResourceReference(typeof(Texture));
    //public KResourceReference downResource = new KResourceReference(typeof(Texture));
    public Texture frontTex;
    public Texture leftTex;
    public Texture rightTex;
    public Texture backTex;
    public Texture upTex;
    public Texture downTex;
}