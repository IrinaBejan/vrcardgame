using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MovieBase: MonoBehaviour
{
    void Start()
    {
        StartCoroutine(CoroutinePlayMovie());
    }  
  
    private IEnumerator CoroutinePlayMovie() 
    {
        Handheld.PlayFullScreenMovie ("Overthrone-Intro.mov", Color.black, FullScreenMovieControlMode.CancelOnInput);
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
        yield break;
    }
}