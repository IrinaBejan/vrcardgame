﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI.Extensions;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TextButtonSelected : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler,IPointerClickHandler
{

    List<BestFitOutline> outlines;
    public Button baseButton;

	void Start()
    {
        var outlinesArray = this.GetComponents<BestFitOutline>();
        outlines = new List<BestFitOutline>(outlinesArray);
        SwitchOutline(false);
    }

    public void SwitchOutline(bool enabled)
    {
        foreach (var outline in outlines)
            outline.enabled = enabled;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        SwitchOutline(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        SwitchOutline(false);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        SwitchOutline(false);

        baseButton.onClick.Invoke();
    }
}
