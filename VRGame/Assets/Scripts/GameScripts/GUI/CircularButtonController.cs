﻿using SprocketTools.Base;
using UnityEngine;
using UnityEngine.UI;


public abstract class CircularButtonController : KMonoBehaviour
{
    public Image progressMeter;

    public float activationTime;

    public Collider colliderComponent;

    public Button buttonComponent;

    public Image imageComponent;

    private float fillAmountTimer;

    public abstract void Action();

    public virtual void Awake()
    {
        if (progressMeter == null)
            progressMeter = GetComponentInChildren<Image>();

        if (colliderComponent == null)
            colliderComponent = GetComponentInChildren<Collider>();

        if (buttonComponent == null)
            buttonComponent = GetComponentInChildren<Button>();

        if (imageComponent == null)
            imageComponent = GetComponent<Image>();

        fillAmountTimer = 0.0f;
    }

    public virtual void Update()
    {
        if ((buttonComponent == null) || (!buttonComponent.interactable))
            return;

        if (SceneManager.TouchModeEnabled)
        {
#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit touchHit;
                Ray touchRay = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(touchRay, out touchHit, 500) && (touchHit.collider == colliderComponent))
                {
                    Action();
                }
            }
#else
			if (Input.touchCount == 1)
			{
				if (Input.GetTouch(0).phase == TouchPhase.Began)
				{
					RaycastHit touchHit;
					Ray touchRay = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

					if (Physics.Raycast(touchRay, out touchHit, 500) && (touchHit.collider == colliderComponent))
					{
						Action();
					}
				}
			}
#endif
        }
        else
        {
            RaycastHit hit;
            Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));

            if (Physics.Raycast(ray, out hit, 500) && (hit.collider == colliderComponent))
            {
                fillAmountTimer += Time.deltaTime;

                progressMeter.fillAmount = fillAmountTimer / activationTime;

                if (fillAmountTimer > activationTime)
                {
                    Action();

                    fillAmountTimer = activationTime;
                }

                if (progressMeter.fillAmount >= 1)
                {
                    fillAmountTimer = 0f;
                    progressMeter.fillAmount = 0f;
                }
            }
            else
            {
                fillAmountTimer -= Time.deltaTime;

                if (fillAmountTimer < 0f)
                    fillAmountTimer = 0f;

                progressMeter.fillAmount = fillAmountTimer / activationTime;
            }
        }
    }
}
