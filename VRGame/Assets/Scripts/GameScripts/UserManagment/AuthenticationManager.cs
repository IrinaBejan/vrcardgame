﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using System;

public static class AuthenticationManager
{
    public const string level_key = "LEVEL_KEY";
    public const string experience_key = "EXPERIENCE_KEY";
    public const string wonMatches_key = "WON_MATHCES_KEY";
    public const string lostMatches_key = "LOST_MATCHES_KEY";
    public const string gameId_key = "PLAYFAB_GAME_ID_KEY";
    public const string appId_key = "PHOTON_APP_ID_KEY";

    public delegate void LoginFullCallback(List<ItemInstance> inventory, string name, int currency, int lvl, int exp, bool buyer);
    public delegate void LoginCallback(string displayName, int level, int exp);
    public delegate void ShowError(PlayFabError error);
    public delegate void ShowSucces();
    public delegate void ShopCallback(List<CatalogItem> shopItems);
    public delegate void BuyCallback(List<ItemInstance> boughtItems);

    public static ShopCallback OnCatalogLoaded;
    public static ShowError ErrorView;
    public static ShowSucces SuccesView;
    public static LoginFullCallback OnLoginCompletedCallback = UpdateData;
    public static BuyCallback OnBuySuccess;

    [SerializeField]
    static string playFabGameID;
    [SerializeField]
    static string appID;

    public static Dictionary<string, string> titleData { get; private set; }

    static string playerID;

    public static string PlayerUsername { get; private set; }
    public static string PlayerPhotonToken { get; private set; }

    private static bool initialized = false;

    private static List<CatalogItem> catalogItems;

    public static void Init()
    {
        if (!initialized)
        {
            initialized = true;
            playFabGameID = LocManager.Get(gameId_key);
            appID = LocManager.Get(appId_key);
        }
        else
            return;
    }

    public static void Register(string username, string password, string email)
    {
        PlayFabSettings.TitleId = playFabGameID;

        RegisterPlayFabUserRequest req = new RegisterPlayFabUserRequest();
        req.TitleId = playFabGameID;
        req.Username = username;
        req.Password = password;
        req.Email = email;

        PlayFabClientAPI.RegisterPlayFabUser(req, OnRegistrationCompleted, OnLoginError);
    }

    internal static void OnRegistrationCompleted(RegisterPlayFabUserResult result)
    {
        playerID = result.PlayFabId;
        PlayerUsername = result.Username;

        Dictionary<string, string> playerData = new Dictionary<string, string>();
        playerData.Add(LocManager.Get(level_key), "0");
        playerData.Add(LocManager.Get(experience_key), "0");
        UpdatePlayerData(playerData);
    }

    public static void Login(string username, string password, bool rememberMe)
    {
        PlayFabSettings.TitleId = playFabGameID;

        LoginWithPlayFabRequest req = new LoginWithPlayFabRequest();
        req.TitleId = playFabGameID;
        req.Username = username;
        req.Password = password;

        if (rememberMe)
        {
            PlayerPrefs.SetString("Username", username);
            PlayerPrefs.SetString("Token", password);
        }
        PlayFabClientAPI.LoginWithPlayFab(req, OnLoginCompleted, OnLoginError);
    }

    internal static void OnLoginError(PlayFabError error)
    {
        Debug.Log("Login error: " + error.Error + " " + error.ErrorMessage);
        PlayerPrefs.DeleteAll();
        ErrorView(error);
    }

    public static void Logout()
    {
        playerID = "";
        PlayerUsername = "";
        PlayerPrefs.DeleteAll();
    }

    internal static void OnLoginCompleted(LoginResult result)
    {
        SuccesView();
        playerID = result.PlayFabId;
        LoadTitleData();

        GetPhotonAuthenticationTokenRequest tokenrequest = new GetPhotonAuthenticationTokenRequest();
        tokenrequest.PhotonApplicationId = appID;
        PlayFabClientAPI.GetPhotonAuthenticationToken(tokenrequest, OnPhotonAuthenticationSuccess, OnError);

        Dictionary<string, string> playerData = new Dictionary<string, string>();

        if (result.NewlyCreated)
        {
            playerData.Add(LocManager.Get(level_key), "0");
            playerData.Add(LocManager.Get(experience_key), "0");
        }

        UpdatePlayerData(playerData);
    }

    private static void UpdatePlayerData(Dictionary<string, string> playerData)
    {
        UpdateUserDataRequest req = new UpdateUserDataRequest();
        req.Data = playerData;
        req.Permission = UserDataPermission.Public;

        PlayFabClientAPI.UpdateUserData(req, OnAddDataSuccess, OnError);
    }

    internal static void OnAddDataSuccess(UpdateUserDataResult result)
    {
        UserUpdate();
    }

    internal static void OnPhotonAuthenticationSuccess(GetPhotonAuthenticationTokenResult result)
    {
        Debug.Log("Photon authentication was successful !");
        PlayerPhotonToken = result.PhotonCustomAuthenticationToken;
    }

    internal static void UserUpdate()
    {
        UpdateUserTitleDisplayNameRequest req = new UpdateUserTitleDisplayNameRequest();
        req.DisplayName = UserManager.DisplayName;

        PlayFabClientAPI.UpdateUserTitleDisplayName(req, OnUserUpdated, OnError);
    }

    internal static void OnUserUpdated(UpdateUserTitleDisplayNameResult result)
    {
        UserManager.DisplayName = result.DisplayName;

        PlayFabClientAPI.GetUserCombinedInfo(new GetUserCombinedInfoRequest(), OnGetUserInformationResult, OnGetUserInformationError);
    }


    private static void OnGetUserInformationResult(GetUserCombinedInfoResult result)
    {
        playerID = result.PlayFabId;
        PlayerUsername = result.AccountInfo.Username;
        int currency = result.VirtualCurrency["1"];

        if (!result.Data.ContainsKey(LocManager.Get(level_key)))
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add(LocManager.Get(level_key), "0");
            data.Add(LocManager.Get(experience_key), "0");

            UpdatePlayerData(data);
        }
        else
        {
            int level = int.Parse(result.Data[LocManager.Get(level_key)].Value);
            int exp = int.Parse(result.Data[LocManager.Get(experience_key)].Value);

            GetStats();
            OnLoginCompletedCallback(result.Inventory, UserManager.DisplayName, currency, level, exp, UserManager.Buyer);
        }
    }

    private static void OnGetUserInformationError(PlayFabError error)
    {
        Debug.Log(error.Error + " " + error.ErrorMessage);

        Dictionary<string, string> playerData = new Dictionary<string, string>();
        playerData.Add(LocManager.Get(level_key), "0");
        playerData.Add(LocManager.Get(experience_key), "0");

        UpdatePlayerData(playerData);
    }

    public static void GetStats()
    {
        GetUserStatisticsRequest req = new GetUserStatisticsRequest();
        PlayFabClientAPI.GetUserStatistics(req, OnGetStats, OnError);
    }

    static void OnGetStats(GetUserStatisticsResult result)
    {
        int lost, won;
        result.UserStatistics.TryGetValue(LocManager.Get(lostMatches_key), out lost);
        result.UserStatistics.TryGetValue(LocManager.Get(wonMatches_key), out won);
        UserManager.UpdateStats(lost, won);
    }

    private static void UpdateData(List<ItemInstance> inventory, string name, int currency, int lvl, int exp, bool buyer)
    {
        UserManager.userInventory = inventory;
        UserManager.DisplayName = name;
        UserManager.UserLevel = lvl;
        UserManager.UserExperience = exp;
        UserManager.Buyer = buyer;
        UserManager.Currency = currency;
    }

    public static void UpdateExperienceLevel(int accountExp, int accountLevel)
    {
        Dictionary<string, string> playerData = new Dictionary<string, string>();
        playerData.Add(LocManager.Get(experience_key), accountExp.ToString());
        playerData.Add(LocManager.Get(level_key), accountLevel.ToString());

        UpdateUserDataRequest req = new UpdateUserDataRequest();
        req.Data = playerData;
        req.Permission = UserDataPermission.Public;

        PlayFabClientAPI.UpdateUserData(req, OnUpdateAccountExpAndLevelSuccess, OnError);
    }

    internal static void OnUpdateAccountExpAndLevelSuccess(UpdateUserDataResult result)
    {
    }

    public static void ReportLostMatches(int number)
    {
        UpdateUserStatisticsRequest req = new UpdateUserStatisticsRequest();
        req.UserStatistics = new Dictionary<string, int>();
        req.UserStatistics.Add(LocManager.Get(lostMatches_key), number);

        PlayFabClientAPI.UpdateUserStatistics(req, OnUpdateStatsCompleted, OnError);
    }

    public static void ReportWonsMatches(int number)
    {
        UpdateUserStatisticsRequest req = new UpdateUserStatisticsRequest();
        req.UserStatistics = new Dictionary<string, int>();
        req.UserStatistics.Add(LocManager.Get(wonMatches_key), number);

        PlayFabClientAPI.UpdateUserStatistics(req, OnUpdateStatsCompleted, OnError);
    }

    internal static void OnUpdateStatsCompleted(UpdateUserStatisticsResult result)
    {
    }

    internal static void OnError(PlayFabError error)
    {
        Debug.LogError(error.Error + " " + error.ErrorMessage);
    }

    public static void LoadTitleData()
    {
        GetTitleDataRequest req = new GetTitleDataRequest();
        req.Keys = new List<string>() { "Key_exp", "Key_lvl" };

        PlayFabClientAPI.GetTitleData(req, OnLoadTitleData, OnError);
    }

    private static void OnLoadTitleData(GetTitleDataResult result)
    {
        titleData = result.Data;
    }


    public static string GetTitleValue(string key)
    {
        string value = "";
        if (titleData != null)
        {
            titleData.TryGetValue(key, out value);
        }
        return value;
    }

    public static void GetCatalog(ShopCallback CatalogCallback)
    {
        OnCatalogLoaded = CatalogCallback;

        if (catalogItems == null)
        {
            GetCatalogItemsRequest req = new GetCatalogItemsRequest();
            req.CatalogVersion = "Test";
            PlayFabClientAPI.GetCatalogItems(req, OnGetCatalog, OnError);
        }
        else
        {
            OnCatalogLoaded(catalogItems);
        }
    }

    static void OnGetCatalog(GetCatalogItemsResult result)
    {
        catalogItems = result.Catalog;

        OnCatalogLoaded(catalogItems);
    }

    public static void BuyItem(CatalogItem item, BuyCallback OnBuyItem)
    {
        OnBuySuccess = OnBuyItem;

        PurchaseItemRequest req = new PurchaseItemRequest();
        req.ItemId = item.ItemId;
        req.Price = (int)item.VirtualCurrencyPrices["1"];
        req.VirtualCurrency = "1";

        PlayFabClientAPI.PurchaseItem(req, OnBuyItemSucc, OnError);
    }
    
    static void OnBuyItemSucc(PurchaseItemResult result)
    {
        OnBuySuccess(result.Items);
    }
}