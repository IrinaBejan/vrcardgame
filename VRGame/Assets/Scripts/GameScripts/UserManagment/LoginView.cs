﻿using UnityEngine;
using System.Collections;
using PlayFab;
using UnityEngine.UI;

public class LoginView : MonoBehaviour
{
    public const string accountNotFound_key = "ACCOUNT_NOT_FOUND_KEY";
    public const string accountBanned_key = "ACCOUNT_BANNED_KEY";
    public const string invalidPassword_key = "INVALID_PASSWORD_KEY";
    public const string invalidUsername_key = "INVALID_USERNAME_KEY";
	public const string emptyFields_key = "EMPTY_FIELDS_KEY";

    public Text errorLabel;
    public InputField userNameField;
    public InputField passwordField;

    public Toggle rememberMe;
    public string Username { get { return userNameField.text; } }
    public string Password { get { return passwordField.text; } }
   
    public void Awake()
    {
        Reset();
    }

    public void Reset()
    {
        rememberMe.enabled = false;
        userNameField.text = "";
        passwordField.text = "";
    }

    public void Login()
    {
		if (userNameField.text.Length > 0 && passwordField.text.Length > 0) {
			AuthenticationManager.ErrorView = OnPlayFabError;
			AuthenticationManager.SuccesView = OnSuccesLogin;
			AuthenticationManager.Login (Username, Password, rememberMe.isOn);
		} 
		else 
		{
			errorLabel.text = LocManager.Get (emptyFields_key);
            SceneManager.SwitchToMenu();//FOR DEBUG PURPOSES
		}
    }
    
    void OnSuccesLogin()
    {
        SceneManager.SwitchToMenu();
    }

    void OnPlayFabError(PlayFabError error)
    {
        Debug.Log("Got an error: " + error.Error);
        Reset();
        if (error.Error == PlayFabErrorCode.InvalidParams && error.ErrorDetails.ContainsKey("Password"))
        {
            errorLabel.text = LocManager.Get(invalidPassword_key);
        }
        else if (error.Error == PlayFabErrorCode.InvalidParams && error.ErrorDetails.ContainsKey("Username"))
        {
            errorLabel.text = LocManager.Get(invalidUsername_key);
        }
        else if (error.Error == PlayFabErrorCode.AccountNotFound)
        {
            errorLabel.text = LocManager.Get(accountNotFound_key);
        }
        else if (error.Error == PlayFabErrorCode.AccountBanned)
        {
            errorLabel.text = LocManager.Get(accountBanned_key);
        }
        else if (error.Error == PlayFabErrorCode.InvalidUsernameOrPassword)
        {
            errorLabel.text = LocManager.Get(accountNotFound_key);
        }
        else
        {
            errorLabel.text = "Unknown Error.";
        }
    }
}
