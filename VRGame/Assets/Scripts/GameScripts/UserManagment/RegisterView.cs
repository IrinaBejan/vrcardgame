﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine.UI;

public class RegisterView : MonoBehaviour
{
    public const string emailNotAvailable_key = "EMAIL_NOTAVAILABLE_KEY";
    public const string usernameNotAvailable_key = "USERNAME_NOTAVAILABLE_KEY";
    public const string invalidPassword_key = "INVALID_PASSWORD_KEY";
    public const string invalidUsername_key = "INVALID_USERNAME_KEY";
    public const string invalidEmail_key = "INVALID_EMAIL_KEY";

    public InputField username;
    public InputField password;
    public InputField confirmedPassword;
    public InputField email;
    public Text errorLabel;

    public void Awake()
    {
        Reset();
    }

    public void Reset()
    {
        username.text = "";
        password.text = "";
        confirmedPassword.text = "";
        email.text = "";
    }
    public void Register()
    {
        if (!ValidateEmail())
        {
            Reset();
            errorLabel.text = LocManager.Get(invalidEmail_key);
        }
        else if (!ValidatePassword())
        {
            Reset();
            errorLabel.text = LocManager.Get(invalidPassword_key);
        }
        else
        {
            AuthenticationManager.ErrorView = OnPlayFabError;
            AuthenticationManager.Register(username.text, password.text, email.text);
        }
    }
    
    private bool ValidateEmail()
    {
        string pattern = @"^([0-9a-zA-Z]([\+\-_\.][0-9a-zA-Z]+)*)+@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,17})$";
        return Regex.IsMatch(email.text, pattern);
    }

    private bool ValidatePassword()
    {
        return (password.text == confirmedPassword.text);
    }
    
    void OnPlayFabError(PlayFabError error)
    {
        Debug.Log("Got an error: " + error.Error);
        Reset();

        if ((error.Error == PlayFabErrorCode.InvalidParams && error.ErrorDetails.ContainsKey("Password")) || (error.Error == PlayFabErrorCode.InvalidPassword))
        {
            errorLabel.text = LocManager.Get(invalidPassword_key);
        }
        else if ((error.Error == PlayFabErrorCode.InvalidParams && error.ErrorDetails.ContainsKey("Username")) || (error.Error == PlayFabErrorCode.InvalidUsername))
        {
            errorLabel.text = LocManager.Get(invalidPassword_key);
        }
        else if (error.Error == PlayFabErrorCode.EmailAddressNotAvailable)
        {
            errorLabel.text = LocManager.Get(emailNotAvailable_key);
        }
        else if (error.Error == PlayFabErrorCode.UsernameNotAvailable)
        {
            errorLabel.text = LocManager.Get(usernameNotAvailable_key);
        }
    }
}