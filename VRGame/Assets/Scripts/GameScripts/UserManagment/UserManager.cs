﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PlayFab.ClientModels;

public static class UserManager
{
    
    private static int[] levelExp = new int[] { 0, 100, 250, 350, 600, 950, 1450, 2400,
                                                3850, 6250, 10100, 16350, 26450, 42800 };
    
    public static int[] ExperienceLevel
    {
        get { return levelExp; }
    }

    private static string displayName;
    public static string DisplayName
    {
        get
        {
            if(displayName == "")
            {
                Debug.Log("Name not set!");
            }
            return displayName;
        }
        set
        {
            displayName = value;
        }
    }

    private static int userLevel;
    public static int UserLevel
    {
        get
        {
            return userLevel;
        }
        set { userLevel = value; }
    }

    private static int userExp;
    public static int UserExperience
    {
        get
        {
            return userExp;
        }
        set
        {
            userExp = value;
        }
    }
    private static int lost;
    private static int won;
    private static int userCurrency;
    public static int LostMatches
    {
        get
        {
            return lost;
        }
        set
        {
            lost = value;
        }
    }

    public static int WonMatches
    {
        get
        {
            return won;
        }
        set
        {
            won = value;
        }
    }

    public static int Currency
    {
        get
        {
            return userCurrency;
        }
        set
        {
            userCurrency = value;
        }
    }

    public static bool Buyer { get; set; }

    public static List<ItemInstance> userInventory;
 
    public static void UpdateStats(int lost, int won)
    {
        LostMatches = lost;
        WonMatches = won;
    }
    internal static void Bought()
    {
        Buyer = true;
    }
     
    public static void GetExperience(int exp)
    {
        UserExperience += exp;
        UpdateLevel();
    }

    private static void UpdateLevel()
    {
        if (UserLevel == levelExp.Length - 1) return;

        int nextExp = levelExp[UserLevel + 1];
        if (UserExperience > nextExp)
        {
            UserLevel++;
            AuthenticationManager.UpdateExperienceLevel(UserExperience, UserLevel);
        }
    }
}