﻿using UnityEngine;
using System.Collections;

[DisallowMultipleComponent]
[RequireComponent(typeof(Animator))]
public class CharacterPhysicsController : MonoBehaviour
{
    public LayerMask walkableLayers;
    public string animatorJumpBoolName = "Jump";
    public string animatorIsGroundedBoolName = "IsGrounded";
    public string animatorJumpLandState = "Jump.LandJump";
    protected int animatorJumpBoolId;
    protected int animatorIsGroundedBoolId;
    protected int animatorJumpLandStateId;

    public float landAnimDelayUntilGrounded = 0.09f;
    public float landAnimTransitionTime = 0.2f;

    public float landAnimPredictionHeight = 2f;
    private float fallHeight = 0f;
    private bool landAnimationStarted = false;

    public float feetColliderRadius = 0.25f;

    public float jumpVelocityAmount = 10f;
    public float jumpForwardForceAmount = 30f;
    public float inAirControlForceAmount = 80f;
    public float inAirTurnSpeed = 3f;
    public float inAirControlDelay = 1.5f;
    public float inAirRigidbodyDrag = 0.25f;

    protected float inAirControlTimer = 0f;
    /// TODO: works only with Vector3.u
    public Vector3 jumpDirection = Vector3.up;

    public float extraGravityFactor = 1.5f;

    protected Rigidbody currentMovingPlatform;

    protected float inputJumpPowerFactor;
    protected float forwardSpeedBeforeJump = 0f;
    protected float defaultTurnSpeed;

    public System.Func<bool> canJumpEvaluateFunc;
    protected bool isJumping;
    protected bool doJump;

    [System.NonSerialized]
    public RootLocomotion locomotion;

    [System.NonSerialized]
    public Animator animator;

    [System.NonSerialized]
    public Rigidbody cachedRigidbody;

    protected GroundSensorListener groundSensor;

    public void Awake()
    {
        gameObject.CheckAndInitializeWithInterface(ref cachedRigidbody);
        gameObject.CheckAndInitializeWithInterface(ref animator);
        gameObject.CheckAndInitializeWithInterface(ref locomotion);

        groundSensor = GetComponentInChildren<GroundSensorListener>();
        if (groundSensor != null)
        {
            if (!groundSensor.GetComponent<Collider>().isTrigger)
            {
                Debug.LogError("CharacterPhysicsController: GroundSensorListener found is not a trigger: " + gameObject.name);
            }

            groundSensor.OnGroundSensorStateChange += HandleOnGroundSensorStateChange;
            groundSensor.OnGroundSensorEnter += HandleOnGroundSensorEnter;
            groundSensor.OnGroundSensorExit += HandleOnGroundSensorExit;
        }
        else
        {
            Debug.LogError("CharacterPhysicsController: GroundSensorListener was " +
                            "not found in the children of " + gameObject.name, gameObject);
        }

        if (canJumpEvaluateFunc == null)
            canJumpEvaluateFunc = CanJumpEvaluator;

        animatorJumpBoolId = Animator.StringToHash(animatorJumpBoolName);
        animatorIsGroundedBoolId = Animator.StringToHash(animatorIsGroundedBoolName);
        animatorJumpLandStateId = Animator.StringToHash(animatorJumpLandState);
    }

    public void Start()
    {
        defaultTurnSpeed = locomotion.turnSpeedFactorTowardTarget;
    }

    public float ForwardSpeed
    {
        get { return Vector3.Dot(cachedRigidbody.velocity, cachedRigidbody.rotation * Vector3.forward); }
    }

    public Vector3 CurrentVelocity
    {
        get { return cachedRigidbody.velocity; }
    }

    public bool IsJumping
    {
        get { return isJumping; }
        protected set
        {
            isJumping = value;
        }
    }

    public bool IsGrounded
    {
        get { return groundSensor.IsGroundSensorTriggered; }
    }

    public bool CanJump
    {
        get
        {
            var evaluator = canJumpEvaluateFunc;
            if (evaluator == null)
            {
                evaluator = canJumpEvaluateFunc = CanJumpEvaluator;
            }

            return evaluator();
        }
    }

    protected bool CanJumpEvaluator()
    {
        return !IsJumping && IsGrounded;
    }

    protected void AttachToMovingPlatform(Rigidbody movingPlatform)
    {
        currentMovingPlatform = movingPlatform;

        Vector3 backupLocalScale = transform.localScale;
        transform.parent = currentMovingPlatform.transform;
        transform.localScale = backupLocalScale;
    }

    protected void DetachFromCurrentMovingPlatform()
    {
        if (currentMovingPlatform != null)
        {
            currentMovingPlatform = null;

            Vector3 backupLocalScale = transform.localScale;
            transform.parent = null;
            transform.localScale = backupLocalScale;
        }
    }

    void HandleOnGroundSensorStateChange(GroundSensorListener sender, bool _isGrounded)
    {
        animator.SetBool(animatorIsGroundedBoolId, _isGrounded);

        if (_isGrounded)
        {
            locomotion.turnSpeedFactorTowardTarget = defaultTurnSpeed;
            cachedRigidbody.drag = 0f;

            landAnimationStarted = false;
        }
    }

    void HandleOnGroundSensorEnter(GroundSensorListener sender, Collider collider)
    {
       //future moving columns
      /*  if (((1 << collider.gameObject.layer) & walkableLayers.value) != 0)
        {
            Rigidbody physicsBody = collider.GetComponent<Rigidbody>();
            if (physicsBody != null && physicsBody.isKinematic)
                AttachToMovingPlatform(collider.GetComponent<Rigidbody>());
        }*/
    }

    void HandleOnGroundSensorExit(GroundSensorListener sender, Collider collider)
    {
        if (currentMovingPlatform != null && collider.GetComponent<Rigidbody>() == currentMovingPlatform)
        {
            DetachFromCurrentMovingPlatform();
        }
    }

    public void Jump(float jumpPowerFactor)
    {
        doJump = true;
        inputJumpPowerFactor = jumpPowerFactor;
        animator.SetBool(animatorJumpBoolId, true);

        DetachFromCurrentMovingPlatform();
    }

    protected void PredictLandAnimationTransition()
    {
        RaycastHit hitInfo;
        if (Physics.SphereCast(transform.position, feetColliderRadius, -transform.up, out hitInfo,
                               landAnimPredictionHeight, walkableLayers.value))
        {
            fallHeight = hitInfo.distance;

            float predictedLandAnimStartHeight = landAnimDelayUntilGrounded * -CurrentVelocity.y;

            if (!landAnimationStarted && fallHeight <= Mathf.Max(0f, predictedLandAnimStartHeight))
            {
                landAnimationStarted = true;
                if (predictedLandAnimStartHeight > 1e-5f)
                {
                    animator.CrossFade(animatorJumpLandStateId, landAnimTransitionTime, 0, 1f - fallHeight / predictedLandAnimStartHeight);
                }
                else
                    animator.CrossFade(animatorJumpLandStateId, landAnimTransitionTime, 0, 1f);
            }
        }
    }
    protected void UpdateGroundedState()
    {
        if (IsJumping && CurrentVelocity.y <= 0.5f * jumpVelocityAmount)
        {
            groundSensor.SetSensorActiveState(true);

            animator.SetBool(animatorJumpBoolId, false);

            if (IsGrounded)
                IsJumping = false;
        }
    }

    protected void UpdateJumpState()
    {
        if (doJump && CanJump)
        {
            forwardSpeedBeforeJump = ForwardSpeed;

            cachedRigidbody.AddForce(jumpDirection * jumpVelocityAmount * inputJumpPowerFactor, ForceMode.VelocityChange);

            IsJumping = true;
            doJump = false;
            groundSensor.SetSensorActiveState(false);
            inAirControlTimer = inAirControlDelay;
        }

        if (!IsGrounded)
        {
            cachedRigidbody.drag = inAirRigidbodyDrag;
            locomotion.turnSpeedFactorTowardTarget = inAirTurnSpeed;
            float inAirControlFactor = inAirControlTimer / inAirControlDelay;

            if (IsJumping)
            {
                Vector3 forwardJumpForce = transform.forward * forwardSpeedBeforeJump * jumpForwardForceAmount * inAirControlFactor;
                cachedRigidbody.AddForce(forwardJumpForce, ForceMode.Acceleration);
            }

            Vector3 inAirControlForce = Vector3.ClampMagnitude(locomotion.TargetVelocity, 1f) * inAirControlForceAmount * inAirControlFactor;
            cachedRigidbody.AddForce(inAirControlForce, ForceMode.Acceleration);

            inAirControlTimer = Mathf.Max(0f, inAirControlTimer - Time.deltaTime);

            if (CurrentVelocity.y <= 0f && !landAnimationStarted)
                PredictLandAnimationTransition();
        }
    }

    protected void UpdateGravity()
    {
        if (!IsGrounded)
            cachedRigidbody.AddForce(Physics.gravity * extraGravityFactor, ForceMode.Acceleration);
    }

    protected void OnAnimatorMove()
    {
        Vector3 moveVelocity = animator.velocity;

        moveVelocity.y = cachedRigidbody.velocity.y;
        cachedRigidbody.velocity = moveVelocity;
    }

    protected void FixedUpdate()
    {
        UpdateGroundedState();
        UpdateJumpState();
        UpdateGravity();
    }
}
