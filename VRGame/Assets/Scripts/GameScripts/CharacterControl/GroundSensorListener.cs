﻿using UnityEngine;
using System.Collections;

public class GroundSensorListener : MonoBehaviour
{
    public delegate void GroundSensorStateChangeCallback(GroundSensorListener sender, bool isGrounded);
    public delegate void GroundSensorTriggeredCallback(GroundSensorListener sender, Collider collider);

    #region Events
    public event GroundSensorStateChangeCallback OnGroundSensorStateChange;
    public event GroundSensorTriggeredCallback OnGroundSensorEnter;
    public event GroundSensorTriggeredCallback OnGroundSensorExit;
    #endregion Events

    private int groundSensorFlagCount = 0;


    public bool IsGroundSensorTriggered
    {
        get { return groundSensorFlagCount > 0; }
    }

    public void SetSensorActiveState(bool isEnabled)
    {
        if (!isEnabled)
        {
            if (groundSensorFlagCount != 0)
                RaiseOnGroundSensorStateChange(false);

            groundSensorFlagCount = 0;
        }
        gameObject.SetActive(isEnabled);
    }

    protected void OnTriggerEnter(Collider enteredCollider)
    {
        RaiseOnGroundSensorEnter(enteredCollider);

        groundSensorFlagCount++;

        if (groundSensorFlagCount == 1)
            RaiseOnGroundSensorStateChange(true);
    }

    protected void OnTriggerExit(Collider exitedCollider)
    {
        RaiseOnGroundSensorExit(exitedCollider);

        groundSensorFlagCount--;
        if (groundSensorFlagCount <= 0)
        {
            RaiseOnGroundSensorStateChange(false);
            groundSensorFlagCount = 0;
        }
    }

    protected void RaiseOnGroundSensorStateChange(bool isGrounded)
    {
        var ev = OnGroundSensorStateChange;

        if (ev != null)
            ev(this, isGrounded);
    }

    protected void RaiseOnGroundSensorEnter(Collider collider)
    {
        var ev = OnGroundSensorEnter;

        if (ev != null)
            ev(this, collider);
    }

    protected void RaiseOnGroundSensorExit(Collider collider)
    {
        var ev = OnGroundSensorExit;

        if (ev != null)
            ev(this, collider);
    }
}
