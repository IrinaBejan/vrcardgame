﻿using UnityEngine;
using UnityEngine.UI;
using SprocketTools.Base;

public class FullscreenWarningController : KMonoBehaviour
{
	public const string defaultContinueText_key = "DEFAULT_CONTINUE_TEXT";

	public const string androidWarning_key = "ANDROID_DISCLAIMER";
	public const string noGyroWarning_key = "WARNING_NO_GYROSCOPE";

	public Text mainText = null;
	public Text continueText = null;

    public bool IsOpen
	{
		get
		{
			return gameObject.activeSelf;
		}
	}

	public void Awake()
	{

        KGameObjectSingleton<FullscreenWarningController>.SetInstance(this);

		gameObject.SetActive(false);

		if (mainText == null)
		{
			Debug.LogError("Main Text component not set for FullscreenWarningController.");
		}

		if (continueText == null)
		{
			Debug.LogError("Continue Text component not set for FullscreenWarningController.");
		}
	}

	public void Show(string textLocId, string continueTextLocId = null)
	{
        mainText.text = LocManager.Get(textLocId);

		if ((continueTextLocId == null) || (continueTextLocId == string.Empty))
		{
            continueText.text = LocManager.Get(defaultContinueText_key);
		}
		else
		{
            continueText.text = LocManager.Get(continueTextLocId);
		}

		gameObject.SetActive(true);
	}

	public void ShowRaw(string text, string continueTextLocId = null)
	{
		mainText.text = text;

		if ((continueTextLocId == null) || (continueTextLocId == string.Empty))
		{
            continueText.text = LocManager.Get(defaultContinueText_key);
		}
		else
		{
            continueText.text = LocManager.Get(continueTextLocId);
		}

		gameObject.SetActive(true);
	}

	public void Hide()
	{
		gameObject.SetActive(false);
	}
}
