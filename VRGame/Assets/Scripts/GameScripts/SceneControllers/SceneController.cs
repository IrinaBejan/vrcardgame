﻿using SprocketTools.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public partial class SceneController : KMonoBehaviour
{
    public static SceneController Instance = null;

    public SkyboxUnwrapTextures skyboxTextures = null;

    [NonSerialized]
    public AudioSource audioSourceComponent = null;

    [NonSerialized]
    public AudioButtonController audioButton = null;

    readonly public Color menuColor = new Color(1f, 0.27f, 0.27f);

    public float SoundClipProgress
    {
        get
        {
            return audioSourceComponent != null ? Mathf.Clamp01(audioSourceComponent.time / audioSourceComponent.clip.length) : 0f;
        }
    }

    public void PlaySound()
    {
        if (audioSourceComponent != null)
        {
            audioSourceComponent.Play();
        }
    }

    public virtual void Awake()
    {
        Instance = this;

        if (audioSourceComponent == null)
        {
            audioSourceComponent = GetComponentInChildren<AudioSource>();
        }

        if (audioButton == null)
        {
            audioButton = GetComponentInChildren<AudioButtonController>();
        }
    }

    public virtual void Start()
    {
        SceneManager.SetCrossfadeTextures(skyboxTextures,menuColor);

        if (!Application.isPlaying)
        {
            SceneManager.SetCrossfade(SceneManager.CurrentTextureIndex);
        }

        ReloadSound();
    }

    public void ReloadSound()
    {
        if (audioSourceComponent == null)
            return;
        /*
        soundClipPath.Refresh();

        if (soundClipPath != "")
        {
            var soundClip = Resources.Load<AudioClip>((string)soundClipPath);

            audioSourceComponent.clip = soundClip;
        }
   */ }

    public virtual void OnDestroy()
    {
        if ((audioSourceComponent != null) && (IsPlayingSound))
        {
            audioSourceComponent.Stop();

            Resources.UnloadAsset(audioSourceComponent.clip);
        }
        
    }

    public bool IsPlayingSound
    {
        get
        {
            return audioSourceComponent != null ? audioSourceComponent.isPlaying : false;
        }
    }

    internal void StopSound()
    {
        if ((audioButton != null) && (IsPlayingSound))
        {
            audioButton.Flip();
        }

        if (audioSourceComponent != null)
        {
            audioSourceComponent.Stop();

            Resources.UnloadAsset(audioSourceComponent.clip);
        }
    }
}