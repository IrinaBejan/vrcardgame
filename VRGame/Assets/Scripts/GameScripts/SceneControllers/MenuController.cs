﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

using SprocketTools.Base;

public class MenuController : SceneController
{
    public float duration;

    private float timer;

    public override void Awake()
    {
        base.Awake();

        timer = duration;
    }

    public override void Start()
    {
        base.Start();
    }

    public void Update()
    {
        timer -= Time.deltaTime;

    }
}
