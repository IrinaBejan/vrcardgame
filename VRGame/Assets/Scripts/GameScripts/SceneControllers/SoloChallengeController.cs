﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SoloChallengeController : SceneController
{
    [System.Serializable]
    public struct Character
    {
        public GameObject characterPrefab;
        public int nodeNumber;
    }

    [System.Serializable]
    public struct Team
    {
        public Character monarch;
        public Character[] creatures;
    }

    public Character[] Friends { get { return friendlyTeam.creatures; } }
    public Team enemyTeam;
    public Team friendlyTeam;

    public int life;
    public int mana;
   
    public GameObject[] deck;
    public GameObject summonFog;

    public GameUIController gameUI;
    public GameUIController gameUIleft;

    public DeckController deckController;

    private int monarchLife;
    private int opponentLife;
    private int monarchMana;
    private int opponentMana;

    public override void Awake()
    {
        SoloChallengeManager.Init();

        base.Awake();

        SoloChallengeManager.manaY = mana;
        SoloChallengeManager.healthY = life;
        SoloChallengeManager.healthO = life;

        foreach (var character in enemyTeam.creatures)
        {
            character.characterPrefab.tag = "Enemy";
            character.characterPrefab.GetComponent<CharacterCardController>().Node = character.nodeNumber;
            SummonCharacter(character.characterPrefab, character.nodeNumber,-90);
            SoloChallengeManager.boardController.HighlightRed(character.nodeNumber);
        }

        enemyTeam.monarch.characterPrefab.tag = "King Enemy";
        enemyTeam.monarch.characterPrefab.GetComponent<CharacterCardController>().Node = enemyTeam.monarch.nodeNumber;
        SummonCharacter(enemyTeam.monarch.characterPrefab, enemyTeam.monarch.nodeNumber,-90);
        SoloChallengeManager.boardController.HighlightRed(enemyTeam.monarch.nodeNumber);

        foreach (var character in friendlyTeam.creatures)
        {
            character.characterPrefab.tag = "Friend";
            character.characterPrefab.GetComponent<CharacterCardController>().Node = character.nodeNumber;
            SummonCharacter(character.characterPrefab, character.nodeNumber,90);
            SoloChallengeManager.boardController.HighlightGreen(character.nodeNumber);

        }

        friendlyTeam.monarch.characterPrefab.tag = "King Friend";
        friendlyTeam.monarch.characterPrefab.GetComponent<CharacterCardController>().Node = friendlyTeam.monarch.nodeNumber;
        SummonCharacter(friendlyTeam.monarch.characterPrefab, friendlyTeam.monarch.nodeNumber,90);
        SoloChallengeManager.boardController.HighlightGreen(friendlyTeam.monarch.nodeNumber);

        if (SceneManager.currentState == SceneManager.Mode.NonVRMode)
        {
            InitUICamera(SceneManager.centerCamera);
        }
        else if(SceneManager.currentState == SceneManager.Mode.VRMode)
        {
            InitUICamera(SceneManager.rightEyeCamera);
            InitUICamera(SceneManager.leftEyeCamera);
        }
        else if(SceneManager.currentState == SceneManager.Mode.TouchMode)
        {
            InitUICamera(SceneManager.touchCamera);
        }
      
        deckController.currentHand = new List<GameObject>(deck);
        deckController.gameDeck = new List<GameObject>(deck);
        deckController.RedrawHand();
    }

    public void InitUICamera(GameObject obj)
    {
        gameUI = obj.GetComponentInChildren(typeof(GameUIController), true) as GameUIController;
        gameUI.gameObject.SetActive(true);
        gameUI.SetStats(life, mana);
    }

    public void RefreshValues(GameObject obj)
    {
        gameUI = obj.GetComponentInChildren(typeof(GameUIController), true) as GameUIController;
        gameUI.SetLifeOpponent(SoloChallengeManager.healthO);
        gameUI.SetLifeUser(SoloChallengeManager.healthY);
        gameUI.SetManaOpponent(SoloChallengeManager.manaO);
        gameUI.SetManaUser(SoloChallengeManager.manaY);
    }

    public override void Start()
    {
        SceneManager.SetCrossfadeTextures(skyboxTextures, Color.grey);

        if (!Application.isPlaying)
        {
            SceneManager.SetCrossfade(SceneManager.CurrentTextureIndex);
        }

        ReloadSound();
    }

    public void SummonCharacter(GameObject prefab, int targetNode, int yRotation)
    {
        GameObject parent = GameObject.Find("BoardColumn " + targetNode.ToString());
        Vector3 parentPosition = parent.transform.position;
        parentPosition.y += 2;
        Instantiate(summonFog, parentPosition, Quaternion.Euler(0, yRotation, 0));
        Instantiate(prefab, parentPosition, Quaternion.Euler(0, yRotation, 0));
    }

    public static void SummonCharacter(GameObject prefab, int targetNode)
    {
        int yRotation = 90;
        prefab.GetComponent<CharacterCardController>().Node = targetNode;
        prefab.tag = "Friend";
        GameObject parent = GameObject.Find("BoardColumn " + targetNode.ToString());
        Vector3 parentPosition = parent.transform.position;
        parentPosition.y += 2;
        Instantiate(prefab, parentPosition, Quaternion.Euler(0, yRotation, 0));
        SoloChallengeManager.boardController.HighlightGreen(targetNode);

    }


    public void Update()
    {
        SoloChallengeManager.Update();
        monarchLife = SoloChallengeManager.healthY;
        opponentLife = SoloChallengeManager.healthO;
        monarchMana = SoloChallengeManager.manaY;
        opponentMana = SoloChallengeManager.manaO;

        if (SceneManager.currentState == SceneManager.Mode.NonVRMode)
        {
            RefreshValues(SceneManager.centerCamera);
        }
        else if (SceneManager.currentState == SceneManager.Mode.VRMode)
        {
            RefreshValues(SceneManager.rightEyeCamera);
            RefreshValues(SceneManager.leftEyeCamera);
        }
        else if (SceneManager.currentState == SceneManager.Mode.TouchMode)
        {
            RefreshValues(SceneManager.touchCamera);
        }
    }
}

