﻿using UnityEngine;
using System.Collections;

public class PlayModeController : SceneController
{
    public float duration;

    private float timer;

    public override void Awake()
    {
        base.Awake();

        timer = duration;
    }

    public override void Start()
    {
        base.Start();
    }

    public void Update()
    {
        timer -= Time.deltaTime;

    }
}
