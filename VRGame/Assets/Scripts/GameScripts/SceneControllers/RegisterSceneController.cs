﻿using UnityEngine;
using System.Collections;

public class RegisterSceneController : SceneController
{
    private const string usernamePrefs = "Username";
    private const string tokenPrefs = "Token";

    public GameObject loginScreen;

    private float timer;
    private bool loginSuccesful = false;
    private bool availablePrefs = false;

    public override void Start()
    {
        base.Start();
        if (PlayerPrefs.HasKey(usernamePrefs) && PlayerPrefs.HasKey(tokenPrefs))
        {
            availablePrefs = true;

            AuthenticationManager.SuccesView = OnSuccesLogin;

            try
            {
                string username = PlayerPrefs.GetString(usernamePrefs);
                string token = PlayerPrefs.GetString(tokenPrefs);

                AuthenticationManager.Login(username, token, false);
            }
            catch (System.Exception err)
            {
                Debug.LogError("Got: " + err);
            }
        }
    }

    public void Update()
    {
        timer += Time.deltaTime;

        if (availablePrefs)
        {
            if (loginSuccesful)
            {
                SceneManager.SwitchToMenu();
            }
        }
    }

    void OnSuccesLogin()
    {
        loginSuccesful = true;
    }
}