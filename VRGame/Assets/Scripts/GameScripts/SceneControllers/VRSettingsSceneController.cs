﻿using SprocketTools.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class VRSettingsSceneController : SceneController
{
    public static bool showWarning = true;

    public override void Start()
    {
        base.Start();

        if (showWarning)
        {
            showWarning = false;

            string stringToShow = "";

            if (!SystemInfo.supportsGyroscope)
            {
                stringToShow = LocManager.Get(FullscreenWarningController.noGyroWarning_key) + "\n\n";
            }

#if UNITY_ANDROID
            KGameObjectSingleton<FullscreenWarningController>.Instance.Show(FullscreenWarningController.androidWarning_key);

            stringToShow += LocManager.Get(FullscreenWarningController.androidWarning_key);
#endif

            if (stringToShow != string.Empty)
                KGameObjectSingleton<FullscreenWarningController>.Instance.ShowRaw(stringToShow);
        }
    }
}
