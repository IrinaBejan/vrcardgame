﻿using SprocketTools.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class SystemsInitializer : KMonoBehaviour
{
    public void Awake()
    {
        Application.targetFrameRate = 120;
    }

    public void Start()
    {
        LocManager.Load();
        SceneManager.Init();
        AuthenticationManager.Init();
        AudioManager.Init();
    }

    public void Update()
    {
        SceneManager.Update();

        if (!SceneManager.CrossfadeInProgress && ((Application.platform == RuntimePlatform.Android)
                                                || (Application.isEditor)))
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (SceneController.Instance is MenuController)
                {
                    SceneManager.SwitchVR(false);
                    SceneManager.TouchMode(true);
                    SceneManager.SwitchToVRConfigScreen();
                }
                else if (SceneController.Instance is VRSettingsSceneController)
                {
                    SceneManager.Exit();
                }
                else if(SceneController.Instance is SoloChallengeController)
                {
                    SceneManager.Reset(); 
                    SceneManager.SwitchToMenu();
                }
                else if (SceneController.Instance is SceneController)
                {
                    SceneManager.SwitchToMenu();
                }
           
                return;
            }
        }
    }
}
