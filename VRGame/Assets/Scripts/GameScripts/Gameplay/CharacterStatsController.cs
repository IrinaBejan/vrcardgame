﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CharacterStatsController : MonoBehaviour
{
    public Canvas canvas;
    public bool onY = true, onX=false, onZ=false, reversedX=false, reversedY=false, reversedZ=false;
    public void Start()
    {
        canvas = this.gameObject.GetComponent<Canvas>();
        canvas.worldCamera = Camera.main;
    }

    public void Update()
    {
        var rotation = canvas.transform.rotation;
        if(onY)
            rotation.y = Camera.main.transform.rotation.y;
        if(onX)
            rotation.x = Camera.main.transform.rotation.x;
        if (onZ)
            rotation.z = Camera.main.transform.rotation.z;
        if (reversedX)
            rotation.x = 360-Camera.main.transform.rotation.x;
        if (reversedY)
            rotation.y = 360 - Camera.main.transform.rotation.y;
        if (reversedZ)
            rotation.z = 360 - Camera.main.transform.rotation.z;
        canvas.transform.rotation = rotation;
    }
}
