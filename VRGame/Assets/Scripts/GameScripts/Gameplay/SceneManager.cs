﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public static class SceneManager
{
    public static int currentSceneIndex = 0;
    public static UnityEngine.Object[] scenes;

    private static UnityEngine.GameObject currentSceneObject = null;

    public static Texture2D Texture;
    private static int currentTextureIndex = 0;

    public static GameObject leftEyeCamera;
    public static GameObject rightEyeCamera;
    public static GameObject centerCamera;
    public static GameObject touchCamera;

    public static Image eyeProgressLeft;
    public static Image eyeProgressRight;
    public static Image eyeProgressCenter;

    public static Guid lastGuid; 
    public static float fillAmountTimer;
    public static Image progressMeter;

    public enum Mode
    {
        VRMode,
        NonVRMode,
        TouchMode
    }

    public static Mode currentState;

    private static bool touchEnabled = false;
    private static bool vrEnabled = true;

    public static bool VREnabled
    {
        get
        {
            return vrEnabled;
        }
    }

    public static bool TouchModeEnabled
    {
        get
        {
            return touchEnabled;
        }
    }

    internal static void Reset()
    {
        Resources.UnloadUnusedAssets();
     //   UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    internal static void SwitchToCollection()
    {
        ChangeScene(collectionScreen);
    }

    public static GyroController gyroController;
    public static TouchController touchController;

    public static int CurrentTextureIndex
    {
        get
        {
            return currentTextureIndex;
        }
    }

    private const float crossfadeDuration = 1f;
    private static float timer = 0.0f;

    public static bool CrossfadeInProgress
    {
        get { return timer > 0f; }
    }

    private static Material workingMaterial;

    private static UnityEngine.Object menuScreen;
    private static UnityEngine.Object registerScreen;
    private static UnityEngine.Object vrSettingsScreen;
    private static UnityEngine.Object playModes;
    private static UnityEngine.Object collectionScreen;

    internal static void FillEyePlus(float deltaTime, float activationTime)
    {
        fillAmountTimer += deltaTime;
       /* if (eyeProgressCenter == null)
            Log("eye progress center is null");
        if (eyeProgressLeft == null)
            Log("eye progress left is null");
        if (eyeProgressRight == null)
            Log("eye progress right is null");*/
        eyeProgressLeft.fillAmount = fillAmountTimer / activationTime;
        eyeProgressRight.fillAmount = fillAmountTimer / activationTime;
        eyeProgressCenter.fillAmount = fillAmountTimer / activationTime;

        if (fillAmountTimer > activationTime)
        {
            fillAmountTimer = activationTime;
        }

        if (eyeProgressCenter.fillAmount >= 1)
        {
            fillAmountTimer = 0f;

            eyeProgressLeft.fillAmount = 0;
            eyeProgressRight.fillAmount = 0;
            eyeProgressCenter.fillAmount = 0; 
        }
    }

    internal static void SwitchToScene(string scene)
    {
        var scenePrefab = Resources.Load("MenuPrefabs/"+scene);
        ChangeScene(scenePrefab);
    }

    internal static void FillEyeMinus(float deltaTime, float activationTime)
    {
        fillAmountTimer -= deltaTime;

        if (fillAmountTimer < 0f)
            fillAmountTimer = 0f;

        /* if (eyeProgressCenter == null)
             Log("eye progress center is null");
         if (eyeProgressLeft == null)
             Log("eye progress left is null");
         if (eyeProgressRight == null)
             Log("eye progress right is null");
         */
        eyeProgressLeft.fillAmount = fillAmountTimer / activationTime;
        eyeProgressRight.fillAmount = fillAmountTimer / activationTime;
        eyeProgressCenter.fillAmount = fillAmountTimer / activationTime;
    }
    
    private static bool initialized = false;

    public static void Init()
    {
        if (!initialized)
            initialized = true;
        else
            return;

        registerScreen = Resources.Load("MenuPrefabs/RegisterUser");
        menuScreen = Resources.Load("MenuPrefabs/MainMenu");
        vrSettingsScreen = Resources.Load("MenuPrefabs/VRSettings");
        playModes = Resources.Load("MenuPrefabs/PlayMode");
        collectionScreen = Resources.Load("MenuPrefabs/CollectionDeck");

        workingMaterial = new Material(RenderSettings.skybox);

        RenderSettings.skybox = workingMaterial;


        if (leftEyeCamera == null)
        {
            leftEyeCamera = GameObject.Find("Camera/CameraGyroWrapper/Left");
        }

        if (rightEyeCamera == null)
        {
            rightEyeCamera = GameObject.Find("Camera/CameraGyroWrapper/Right");
        }

        if (centerCamera == null)
        {
            centerCamera = GameObject.Find("Camera/CameraGyroWrapper/Center");
        }
        
        if (touchCamera == null)
        {
            touchCamera = GameObject.Find("Camera/CameraGyroWrapper/CenterTouch");
        }

        if(eyeProgressCenter == null)
        {
            eyeProgressCenter = GameObject.Find("Camera/CameraGyroWrapper/Center/Canvas/ButtonSelectGeneral/Button").GetComponent<Image>();
        }

        if (eyeProgressLeft == null)
        {
            eyeProgressLeft = GameObject.Find("Camera/CameraGyroWrapper/Left/Canvas/ButtonSelectGeneral/Button").GetComponent<Image>();
        }

        if (eyeProgressRight == null)
        {
            eyeProgressRight = GameObject.Find("Camera/CameraGyroWrapper/Right/Canvas/ButtonSelectGeneral/Button").GetComponent<Image>();
        }

        if (gyroController == null)
        {
            var obj = GameObject.Find("Camera/CameraGyroWrapper");

            gyroController = obj.GetComponent<GyroController>();

            if (gyroController != null)
                gyroController.enabled = false;
        }

        if (touchController == null)
        {
            var obj = GameObject.Find("Camera/CameraGyroWrapper");
            touchController = obj.GetComponent<TouchController>();
            if (touchController != null)
                touchController.enabled = false;
        }

        fillAmountTimer = 0.0f;
        SwitchVR(false);
        TouchMode(true);

        SetGUIEnabled(false);
        //Check if user is logged in, if it is -> SwitchToVRConfigScreen(); else
        SwitchOnRegistrationPanel();
    }

    public static void EnableGyro(bool enabled)
    {
        gyroController.enabled = enabled;
    }

    internal static void SwitchToMenu()
    {
        ChangeScene(menuScreen);

        if (SceneController.Instance is MenuController)
        {
            
        }
    }

    public static void SetCrossfadeTextures(SkyboxUnwrapTextures target, Color tintColor)
    {
        Init();

        if (timer > 0f)
        {
       //     return;
        }

        currentTextureIndex = (currentTextureIndex + 1) % 2;
        string strTexIndex = currentTextureIndex == 0 ? "" : currentTextureIndex.ToString();

        workingMaterial.SetTexture("_FrontTex" + strTexIndex, target.frontTex);
        workingMaterial.SetTexture("_BackTex" + strTexIndex, target.backTex);
        workingMaterial.SetTexture("_LeftTex" + strTexIndex, target.leftTex);
        workingMaterial.SetTexture("_RightTex" + strTexIndex, target.rightTex);
        workingMaterial.SetTexture("_UpTex" + strTexIndex, target.upTex);
        workingMaterial.SetTexture("_DownTex" + strTexIndex, target.downTex);
        workingMaterial.SetColor("_Tint", tintColor);

        timer = crossfadeDuration;
    }

    public static void Update()
    {
        Init();

        if (timer > 0f)
        {
            timer -= Time.deltaTime;

            if (timer < 0f)
            {
                timer = 0f;
            }

            float factor = (currentTextureIndex == 1) ? 1.0f - Mathf.Clamp01(timer / crossfadeDuration) : Mathf.Clamp01(timer / crossfadeDuration);

            workingMaterial.SetFloat("_Ipo", factor);
        }
    }

    public static void SetCrossfade(float factor)
    {
        Init();

        workingMaterial.SetFloat("_Ipo", factor);
    }

    private static void ChangeScene(UnityEngine.Object scene)
    {
        if (currentSceneObject != null)
            GameObject.Destroy(currentSceneObject);

        currentSceneObject = (GameObject)GameObject.Instantiate(scene);

        if (scene == vrSettingsScreen)
            foreach (Button button in GameObject.FindObjectsOfType<Button>())
            {
                button.GetComponent<Button>().interactable = false;
            }

        if (currentSceneObject != null)
        {
            //TO DO;
        }
    
        Resources.UnloadUnusedAssets();
    }

    internal static void SwitchToVRConfigScreen()
    {
        ChangeScene(vrSettingsScreen);
    }

    internal static void SwitchOnRegistrationPanel()
    {
        ChangeScene(registerScreen);
    }
    public static void SwitchToPlayMode()
    {
        ChangeScene(playModes);
    }

    public static void Exit()
    {
        Application.Quit();
    }

    //TODO
    public static void UpdateCamera()
    {
        Transform trans = GameObject.Find("Camera").transform;
        trans.position = new Vector3(-31.7f, 29.6f, -10.6f);
        trans.rotation = Quaternion.Euler(new Vector3(35f, 90f, 0f));
    }
  
    public static int NumScenes
    {
        get
        {
            return scenes.Length;
        }
    }

    public static void SwitchVR(bool enabled)
    {
        vrEnabled = enabled;

        if (leftEyeCamera != null)
        {
            leftEyeCamera.SetActive(enabled);
        }

        if (rightEyeCamera != null)
        {
            rightEyeCamera.SetActive(enabled);
        }

        if (centerCamera != null)
        {
            centerCamera.SetActive(!enabled);
        }
    }

    public static void TouchMode(bool enabled)
    {
        touchController.enabled = enabled;
        
        if (leftEyeCamera != null)
        {
            leftEyeCamera.SetActive(!enabled);
        }
        if (rightEyeCamera != null)
        {
            rightEyeCamera.SetActive(!enabled);
        }
        if (centerCamera != null)
        {
            centerCamera.SetActive(!enabled);
        }

        if (touchCamera != null)
        {
            touchCamera.SetActive(enabled);
        }

        touchEnabled = enabled;
    }

    public static void SetGUIEnabled(bool enabled)
    {
        foreach (var renderer in gyroController.GetComponentsInChildren<SpriteRenderer>())
        {
            renderer.enabled = enabled;
        }
    }
}
