﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadLevel : MonoBehaviour
{
    private AsyncOperation async = null;
    public Image progressMeter;
    public string levelName;
    
    void Awake()
    {
        if (levelName != null)
            StartCoroutine(LoadALevel(levelName));
        else
            Debug.LogError("Level name not assigned for the loading screen");
    }

    private IEnumerator LoadALevel(string levelName)
    {
        async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(levelName);
        yield return async;
    }
    void OnGUI()
    {
        if (async != null)
        {
            progressMeter.fillAmount = async.progress;
        }
    }
}
