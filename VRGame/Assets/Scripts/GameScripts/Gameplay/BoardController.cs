﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BoardController : MonoBehaviour
{
    [System.Serializable]
    public struct Edge
    {
        public int node1;
        public int node2;
    }

    public enum State
    {
        Default, Red, Green, TemporaryGreen
    }

    public Edge[] edges;
    public UnityEngine.GameObject[] hexColumns;

    public Material defaultMaterial;
    public Material redMaterial;
    public Material greenMaterial;
    public Material temporaryGreenMaterial;

    private List<int>[] hexGraph;
    private State[] columnsState = new State[100];

    void Awake()
    {
        hexGraph = new List<int>[100];
        foreach (var edge in edges)
        {
            if (hexGraph[edge.node1] == null)
            {
                hexGraph[edge.node1] = new List<int>();
            }
            if (hexGraph[edge.node2] == null)
            {
                hexGraph[edge.node2] = new List<int>();
            }

            hexGraph[edge.node1].Add(edge.node2);
            hexGraph[edge.node2].Add(edge.node1);
        }
    }
    private void ChangeMaterial(int node, Material material)
    {
        var meshRenderer = hexColumns[node].GetComponentInChildren<MeshRenderer>();
        meshRenderer.material = material;
    }

    public bool IsGreen(int node)
    {
        return (columnsState[node] == State.Green);
    }

    public bool IsDefault(int node)
    {
        return (columnsState[node] == State.Default);
    }

    public bool IsRed(int node)
    {
        return (columnsState[node] == State.Red);
    }
    public bool IsTemporaryGreen(int node)
    {
        return (columnsState[node] == State.TemporaryGreen);
    }

    public void HighlightAdjacentGreen(int node, int range)
    {
        Queue<int> queue = new Queue<int>();
        queue.Enqueue(node);

        bool[] highlighted = new bool[25];
        highlighted.SetValue(false, 0);

        for (int i = 0; i <= range; i++)
        {
            Queue<int> secQueue = new Queue<int>();
            while (queue.Count != 0)
            {
                int currentNode = queue.Dequeue();
                HighlightTemporaryGreen(currentNode);
                highlighted[currentNode] = true;

                foreach (var adjacentNode in hexGraph[currentNode])
                {
                    if (highlighted[adjacentNode] == false)
                        secQueue.Enqueue(adjacentNode);
                }
            }
            queue = secQueue;
        }
    }

    public void HighlightRed(int node)
    {
        ChangeMaterial(node, redMaterial);
        columnsState[node] = State.Red;
    }

   public void HighlightGreen(int node)
    {
        if (columnsState[node] == State.Default || columnsState[node] == State.TemporaryGreen )
        {
            ChangeMaterial(node, greenMaterial);
            columnsState[node] = State.Green;
        }
    }
    public void HighlightTemporaryGreen(int node)
    {
        if (columnsState[node] == State.Default)
        {
            ChangeMaterial(node, temporaryGreenMaterial);
            columnsState[node] = State.TemporaryGreen;
        }
    }

    internal void ResetHard(int node)
    {
        columnsState[node] = State.Default;
        ChangeMaterial(node, defaultMaterial);
    }

    public void Reset()
    {
        for (int column = 1; column < hexColumns.Length; column++)
            if (IsTemporaryGreen(column))
            {
                ChangeMaterial(column, defaultMaterial);
                columnsState[column] = State.Default;
            }
    }
}
