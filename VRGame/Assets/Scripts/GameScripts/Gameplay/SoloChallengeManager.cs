﻿using UnityEngine;
using System.Collections;

public static class SoloChallengeManager
{
    private static bool initialized = false;

    public static UnityEngine.Object board;
    public static UnityEngine.Object winningDisclaimer;
    public static UnityEngine.Object losingDisclaimer;

    public static BoardController boardController;

    public static CardController cardSelected = null;
    public static GameObject friendlyCharacterSelected = null;
    public static GameObject enemyCharacterSelected = null;
    public static int? tileSelectedNumber = null;

    internal static bool rightOrder = false;

    public static int manaY, manaO, healthY, healthO;

    public static void Init()
    {
        if (!initialized)
            initialized = true;
        else
            return;

        board = Resources.Load("Board");
        winningDisclaimer = Resources.Load("WinningDisclaimer");
        losingDisclaimer = Resources.Load("LosingDisclaimer");

        var boardObj = (GameObject)GameObject.Instantiate(board);
        boardController = boardObj.gameObject.GetComponent<BoardController>();
    }

    internal static void Reset()
    {
        cardSelected = null;
        friendlyCharacterSelected = null;
        enemyCharacterSelected = null;
        tileSelectedNumber = null;
    }

    internal static void SelectTile(int index)
    {
        tileSelectedNumber = index;
        if (friendlyCharacterSelected != null)
            rightOrder = true;
        else if (cardSelected == null)
        {
            Reset();
            boardController.Reset();
        }
    }

    internal static void SelectCard(CardController card)
    {
        Reset();
        cardSelected = card;
    }

    internal static void SelectCharacter(GameObject character)
    {
        CharacterCardController controller = character.gameObject.GetComponent<CharacterCardController>();
        var characterPrefab = controller.characterPrefab;

        if (characterPrefab.tag == "Friend" || characterPrefab.tag == "King Friend")
        {
            boardController.Reset();
            tileSelectedNumber = null;
            friendlyCharacterSelected = character;
            if(controller.timesMoved == 0)
                boardController.HighlightAdjacentGreen(controller.Node, 1);
        }
        else
        {
            enemyCharacterSelected = character;
            if (friendlyCharacterSelected != null)
                rightOrder = true;
            else
                rightOrder = false;
        }
    }

    public static void Update()
    {
        if(cardSelected != null)
        {
            int mana = 0;
            if(cardSelected is SpellCardController)
            {
                var controller = cardSelected.gameObject.GetComponent<SpellCardController>();
                
                SpellCardController.SpellType type = controller.type;
                if (type == SpellCardController.SpellType.NonCreatureSpell)
                {
                    controller.Action();
                    Reset();
                }

                if (friendlyCharacterSelected != null && type == SpellCardController.SpellType.FriendSpell)
                {
                    controller.Action();
                    mana = controller.manaRequired;
                    Reset();
                }
                if (enemyCharacterSelected != null && type == SpellCardController.SpellType.EnemySpell)
                {
                    controller.Action();
                    mana = controller.manaRequired;
                    Reset();
                }
            }
            else if (cardSelected is ArtifactCardController)
            {
               //TODO
            }
            else if (cardSelected is CharacterCardController)
            {
                if (tileSelectedNumber != null)
                {
                    var type = cardSelected.GetComponent<CharacterCardController>();
                    type.Action(tileSelectedNumber);
                    mana = type.manaRequired;
                    
                    Reset();
                }
            }
            manaY -= mana;
        }
        else
        {
            if(friendlyCharacterSelected != null && enemyCharacterSelected != null && rightOrder)
            {
              //  var targetPos = boardController.hexColumns[(int)tileSelectedNumber].transform.position;
              //  targetPos.x -= 5;
               // targetPos.y -= 5;

                var friendlyController = friendlyCharacterSelected.GetComponent<CharacterCardController>();
                if (friendlyController.timesAttacked == 0)
                {
                    var enemyController = enemyCharacterSelected.GetComponent<CharacterCardController>();

                    friendlyController.Health -= enemyController.Attack;

                    if (friendlyCharacterSelected.tag == "King Friend")
                        healthY = friendlyController.Health;

                    if (friendlyController.Health <= 0)
                    {
                        GameObject.Destroy(friendlyCharacterSelected);
                        boardController.ResetHard(friendlyController.Node);
                        if (friendlyCharacterSelected.tag == "King Friend")
                            GameObject.Instantiate(losingDisclaimer);
                        boardController.Reset();
                    }

                    enemyController.Health -= friendlyController.Attack;

                    if (enemyCharacterSelected.tag == "King Enemy")
                        healthO = enemyController.Health;
                    if (enemyController.Health <= 0)
                    {
                        boardController.ResetHard(enemyController.Node);
                        if (enemyCharacterSelected.tag == "King Enemy")
                            GameObject.Instantiate(winningDisclaimer);
                        GameObject.Destroy(enemyCharacterSelected);
                    }

                    friendlyController.timesAttacked++;
                }

                rightOrder = false;
                Reset();
            }
            else if(friendlyCharacterSelected != null && tileSelectedNumber!=null && rightOrder )
            {
                var friendlyController = friendlyCharacterSelected.gameObject.GetComponent<CharacterCardController>();
                if (friendlyController.timesMoved == 0)
                    if (boardController.IsTemporaryGreen((int)tileSelectedNumber))
                    {
                        friendlyCharacterSelected.transform.position = boardController.hexColumns[(int)tileSelectedNumber].transform.position;
                        boardController.ResetHard(friendlyController.Node);
                        friendlyController.Node = (int)tileSelectedNumber;
                        boardController.Reset();
                        boardController.HighlightGreen(friendlyController.Node);
                        friendlyController.timesMoved++;
                    }
                rightOrder = false;
                Reset();
            }
        }
    }
}