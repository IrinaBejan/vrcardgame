﻿using SprocketTools.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class SettingsButtonUtility : KMonoBehaviour
{
    public float timer = 0;
    public float fadeTime = 1f;
    public MaskableGraphic[] fadeImageComponents = null;
    public bool GyroRequired = true;

    private Button button;

    public void Awake()
    {
        button = GetComponent<Button>();

        if (fadeImageComponents == null)
            fadeImageComponents = GetComponentsInChildren<MaskableGraphic>();

        UnityEngine.Color targetColor;
        if (button != null)
        {
            targetColor = button.colors.normalColor;
            targetColor.a = 0f;
        }
        else
            targetColor = Color.black;
       foreach (var comp in fadeImageComponents)
       {
            if(comp!=null)
                comp.color = targetColor;
       }
    }

    public void PlayButton()
    {
        SceneManager.Reset();
    }
    public void Update()
    {
        if (timer <= fadeTime)
        {
            timer += Time.deltaTime;

            var factor = Mathf.Clamp01(timer / fadeTime);

            UnityEngine.Color targetColor;
            if (button != null)
                targetColor = !SystemInfo.supportsGyroscope && GyroRequired ? button.colors.disabledColor : button.colors.normalColor;
            else
                targetColor = Color.black;
            targetColor.a *= factor;

            foreach (var comp in fadeImageComponents)
                if(comp!=null)
                comp.color = targetColor;
        }
        else
        {
            if (button != null)
            {
                button.interactable = SystemInfo.supportsGyroscope || !GyroRequired;
            }
        }
    }

    public void SwitchToCollection()
    {
        SceneManager.SwitchToCollection();
    }

    public void SetVR()
    {
        SceneManager.currentState = SceneManager.Mode.VRMode;
    }
    public void SetNonVR()
    {
        SceneManager.currentState = SceneManager.Mode.NonVRMode;
    }
    public void SetTouch()
    {
        SceneManager.currentState = SceneManager.Mode.TouchMode;
    }

    public void EnableGyro()
    {
        SceneManager.EnableGyro(true);
    }

    public void EnableGUI()
    {
        SceneManager.SetGUIEnabled(true);
    }

    public void SwitchToVRSettings()
    {
        SceneManager.SwitchToVRConfigScreen();
    }
    public void SwitchToPlayMode()
    {
        SceneManager.SwitchToPlayMode();
    }
    public void UpdateCamera()
    {
        SceneManager.UpdateCamera();
    }

    public void SwitchToMainMenu()
    {
        SceneManager.SwitchToMenu();
    }

    public void SwitchToScene(string scene)
    {
        SceneManager.SwitchToScene(scene);
    }

    public void SwitchVR(bool value)
    {
        SceneManager.SwitchVR(value);
    }

    public void SwitchTouchOn()
    {
        SceneManager.TouchMode(true);
    }

    public void Exit()
    {
        SceneManager.Exit();
    }
}