﻿using UnityEngine;
using System.Collections;

public class SelectUtility : MonoBehaviour
{

    public void SelectTile(int index)
    {
        SoloChallengeManager.SelectTile(index);
    }

    public void SelectCharacter(GameObject character)
    {
        SoloChallengeManager.SelectCharacter(character);
    }

    public void SelectCard(CardController card)
    {
        SoloChallengeManager.SelectCard(card);
    }
}
