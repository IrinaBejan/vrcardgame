﻿using UnityEngine;
using System.Collections;
using System;

#if UNITY_EDITOR
    using UnityEditor;
#endif

public class SpellCardController : CardController
{
    public enum SpellType
    {
        NonCreatureSpell,
        FriendSpell,
        EnemySpell
    }
    public SpellType type;

    public override void Action()
    {
#if UNITY_EDITOR
        var scriptAsset = AssetDatabase.FindAssets(base.name);
        if (scriptAsset.Length > 0)
        {
            var action = gameObject.AddComponent(Type.GetType(base.name));
            Debug.Log("Attending to use card named " + base.name);
        }
        else
        {
            Debug.Log("Couldn't find the mentioned card " + base.name);
        }
#endif
    }
}
