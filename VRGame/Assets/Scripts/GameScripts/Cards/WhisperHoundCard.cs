﻿using UnityEngine;
using System.Collections;

public class WhisperHoundCard : MonoBehaviour
{
    public void Start()
    {
         var gameObjects = GameObject.FindGameObjectsWithTag("Friend");
         foreach (var obj in gameObjects)
         {
             var controller = obj.GetComponent<CharacterCardController>();
             controller.Attack = controller.Attack + 2;
         }
    }
}
