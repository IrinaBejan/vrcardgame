﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class DeckController : MonoBehaviour
{
    public List<GameObject> gameDeck;
    public List<GameObject> currentHand;
    public GameObject parent;
    public enum GameType
    {
        SoloChallenge,
        ArenaChallenge,
        PracticeRoom
    }

    public GameType gameType;
    public void Awake()
    {
        if (SceneController.Instance is SoloChallengeController)
        {
            gameType = GameType.SoloChallenge;
        }/*
        else if (SceneController.Instance is ArenaController)
        {
            gameType = GameType.ArenaChallenge;
        }
        else if (SceneController.Instance is PracticeRoomController)
        {
            gameType = GameType.PracticeRoom;
        }*/
    }

    public void Start()
    {
    }

    public void UseCard(Guid guid)
    {
        GameObject card;
        CardController controller;
        for (int it = currentHand.Count - 1; it >= 0; it--)
        {
            card = currentHand[it];
            controller = card.GetComponent<CardController>();
            if (controller.guid == guid)
            {
                controller.Action();
                currentHand.RemoveAt(it);
            }
        }

        if (gameType == GameType.ArenaChallenge || gameType == GameType.PracticeRoom)
            DrawCard();

        RedrawHand();
    }

    public void DrawCard()
    {
        if (gameDeck.Count!=0)
        {
            System.Random rnd = new System.Random();
            int cardIndex = rnd.Next(0, gameDeck.Count-1);
            currentHand.Add(gameDeck[cardIndex]);
            gameDeck.RemoveAt(cardIndex);
        }
    }

    public void RedrawHand()
    {
        var children = new List<GameObject>();
        foreach (Transform child in parent.transform)
            children.Add(child.gameObject);
        children.ForEach(child => Destroy(child));

        foreach(var card in currentHand )
        {
            var obj = Instantiate(card.GetComponent<CardController>().card);
            obj.transform.SetParent(parent.transform);
            obj.transform.localPosition = Vector3.zero;

        }
    }

}
