﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
public class CharacterCardController : CardController
{
    public GameObject characterPrefab;
    public Text attack;
    public Text defense;
    public int range;
    [SerializeField]
    private int health;
    [SerializeField]
    private int attackPower;
    [SerializeField]
    private int node;

    [NonSerialized]
    public int timesMoved = 0;
    [NonSerialized]
    public int timesAttacked = 0;
    public int Health
    {
        get
        {
            return health;
        }
        set
        {
            health = value; defense.text = health.ToString();
        }
    }

    public int Node
    {
        get
        {
            return node;
        }
        set
        {
            node = value; 
        }
    }

    public int Attack
    {
        get
        {
            return attackPower;
        }
        set
        {
            attackPower = value; attack.text = attackPower.ToString();
        }
    }

    public override void Action()
    {
        throw new NotImplementedException();
    }

    public void Action(int? tileNumber)
    {
        SoloChallengeController.SummonCharacter(characterPrefab, (int) tileNumber);
    }
    
}
