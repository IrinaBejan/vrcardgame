﻿using UnityEngine;
using System.Collections;

public class WizardsSlimeCard : SpellCardController
{
	public void Start ()
    {
        var character = SoloChallengeManager.enemyCharacterSelected;
        if (character != null)
        {
            character.tag = "Friend";
            int node = character.GetComponent<CharacterCardController>().Node;
            SoloChallengeManager.boardController.ResetHard(node);
        }
    }
}
