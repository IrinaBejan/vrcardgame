﻿using UnityEngine;
using System.Collections;
using System;
using SprocketTools.Base;

public abstract class CardController : KMonoBehaviour
{
    public string cardName;
    public string cardDescription;
    public string race;

    public int manaRequired;
    public GameObject card;

    public Guid guid;
    public string playFabID;

    public virtual void Awake()
    {
        guid = Guid.NewGuid();
    }

    public abstract void Action();
}
