﻿Shader "VRGame/Skybox/6 Sided Crossfade" {
	Properties{
		_Ipo("Interpolation Factor", Range(0.0, 1.0)) = 0.0
		_Tint("Tint Color", Color) = (.5, .5, .5, .5)
		[Gamma] _Exposure("Exposure", Range(0, 8)) = 1.0
		_Rotation("Rotation", Range(0, 360)) = 0

		[NoScaleOffset] _FrontTex("Front [+Z]   (HDR)", 2D) = "grey" {}
	[NoScaleOffset] _BackTex("Back [-Z]   (HDR)", 2D) = "grey" {}
	[NoScaleOffset] _LeftTex("Left [+X]   (HDR)", 2D) = "grey" {}
	[NoScaleOffset] _RightTex("Right [-X]   (HDR)", 2D) = "grey" {}
	[NoScaleOffset] _UpTex("Up [+Y]   (HDR)", 2D) = "grey" {}
	[NoScaleOffset] _DownTex("Down [-Y]   (HDR)", 2D) = "grey" {}

	[NoScaleOffset] _FrontTex1("Front1 [+Z]   (HDR)", 2D) = "grey" {}
	[NoScaleOffset] _BackTex1("Back1 [-Z]   (HDR)", 2D) = "grey" {}
	[NoScaleOffset] _LeftTex1("Left1 [+X]   (HDR)", 2D) = "grey" {}
	[NoScaleOffset] _RightTex1("Right1 [-X]   (HDR)", 2D) = "grey" {}
	[NoScaleOffset] _UpTex1("Up1 [+Y]   (HDR)", 2D) = "grey" {}
	[NoScaleOffset] _DownTex1("Down1 [-Y]   (HDR)", 2D) = "grey" {}
	}

		SubShader{
		Tags{ "Queue" = "Background" "RenderType" = "Background" "PreviewType" = "Skybox" }
		Cull Off ZWrite Off

		CGINCLUDE
#include "UnityCG.cginc"

		half _Ipo;

	half4 _Tint;
	half _Exposure;
	float _Rotation;

	float4 RotateAroundYInDegrees(float4 vertex, float degrees)
	{
		float alpha = degrees * UNITY_PI / 180.0;
		float sina, cosa;
		sincos(alpha, sina, cosa);
		float2x2 m = float2x2(cosa, -sina, sina, cosa);
		return float4(mul(m, vertex.xz), vertex.yw).xzyw;
	}

	struct appdata_t {
		float4 vertex : POSITION;
		float2 texcoord : TEXCOORD0;
	};
	struct v2f {
		float4 vertex : SV_POSITION;
		float2 texcoord : TEXCOORD0;
	};
	v2f vert(appdata_t v)
	{
		v2f o;
		o.vertex = mul(UNITY_MATRIX_MVP, RotateAroundYInDegrees(v.vertex, _Rotation));
		o.texcoord = v.texcoord;
		return o;
	}

	half4 skybox_frag(v2f i, sampler2D smp, half4 smpDecode)
	{
		half4 tex = tex2D(smp, i.texcoord);
		half3 c = DecodeHDR(tex, smpDecode);
		c = c * _Tint.rgb * unity_ColorSpaceDouble.rgb;
		c *= _Exposure;

		return half4(c, 1);
	}

	half4 blendSkyboxFrag(v2f fragInput, sampler2D srcTex, half4 smpDecode, sampler2D dstTex, half blendFactor)
	{
		half4 srcFrag = skybox_frag(fragInput, srcTex, smpDecode);
		half4 dstFrag = skybox_frag(fragInput, dstTex, smpDecode);

		return dstFrag * _Ipo + srcFrag * (1.0 - blendFactor);
	}
	ENDCG

		Pass{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
		sampler2D _FrontTex;
	half4 _FrontTex_HDR;
	sampler2D _FrontTex1;

	half4 frag(v2f i) : SV_Target
	{
		return blendSkyboxFrag(i, _FrontTex, _FrontTex_HDR, _FrontTex1, _Ipo);
	}
		ENDCG
	}
		Pass{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
		sampler2D _BackTex;
	half4 _BackTex_HDR;
	sampler2D _BackTex1;

	half4 frag(v2f i) : SV_Target
	{
		return blendSkyboxFrag(i, _BackTex, _BackTex_HDR, _BackTex1, _Ipo);
	}
		ENDCG
	}
		Pass{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
		sampler2D _LeftTex;
	half4 _LeftTex_HDR;
	sampler2D _LeftTex1;

	half4 frag(v2f i) : SV_Target
	{
		return blendSkyboxFrag(i, _LeftTex, _LeftTex_HDR, _LeftTex1, _Ipo);
	}
		ENDCG
	}
		Pass{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
		sampler2D _RightTex;
	half4 _RightTex_HDR;
	sampler2D _RightTex1;

	half4 frag(v2f i) : SV_Target
	{
		return blendSkyboxFrag(i, _RightTex, _RightTex_HDR, _RightTex1, _Ipo);
	}
		ENDCG
	}
		Pass{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
		sampler2D _UpTex;
	half4 _UpTex_HDR;
	sampler2D _UpTex1;

	half4 frag(v2f i) : SV_Target
	{
		return blendSkyboxFrag(i, _UpTex, _UpTex_HDR, _UpTex1, _Ipo);
	}
		ENDCG
	}
		Pass{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
		sampler2D _DownTex;
	half4 _DownTex_HDR;
	sampler2D _DownTex1;

	half4 frag(v2f i) : SV_Target
	{
		return blendSkyboxFrag(i, _DownTex, _DownTex_HDR, _DownTex1, _Ipo);
	}
		ENDCG
	}
	}
}
