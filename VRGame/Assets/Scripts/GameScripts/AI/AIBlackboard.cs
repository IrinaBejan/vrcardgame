﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SprocketTools.Base;

[System.Serializable]
public class AISceneData
{
    public string key;
    public Object dataObject;
}

public class AIBlackboard : KMonoBehaviour
{
    public Animator aiLogicAnimator;

    public List<AISceneData> sceneData;
    protected Dictionary<string, object> data = new Dictionary<string, object>();
    protected Dictionary<int, GameObject> aiTargets = new Dictionary<int, GameObject>(100);

    public Dictionary<string, object> Data
    {
        get { return data; }
    }

    public List<GameObject> AITargets
    {
        get { return aiTargets.Values.ToList(); }
    }

    void Awake()
    {
        if (sceneData != null)
        {
            for (int i = 0; i < sceneData.Count; i++)
                data[sceneData[i].key] = sceneData[i].dataObject;
        }
    }

    void Start()
    {
        InitStateMachineBehaviours();
    }
    
    public void InitStateMachineBehaviours()
    {
        if (aiLogicAnimator == null)
            aiLogicAnimator = GetComponent<Animator>();

        if (aiLogicAnimator != null)
        {
            AAIStateBehaviour[] aiStateBehaviours = aiLogicAnimator.GetBehaviours<AAIStateBehaviour>();

            for (int i = 0; i < aiStateBehaviours.Length; i++)
            {
                aiStateBehaviours[i].aiBlackboard = this;
            }
        }
    }

    public void RegisterAITarget(GameObject newAITarget, int id)
    {
        aiTargets.Add(id, newAITarget);
    }

    public void UnregisterAITarget(int id)
    {
        aiTargets.Remove(id);

    }

    public void CheckAndInitializeFrom<T>(GameObject gameObj, string fieldKey, ref T field) where T : class
    {
        if (field == null)
        {
            object keyValue;
            if (!Data.TryGetValue(fieldKey, out keyValue))
            {
                if (gameObj != null)
                    field = gameObj.GetComponent<T>();

                Data[fieldKey] = field;
            }
            else
                field = keyValue as T;
        }
    }
}
