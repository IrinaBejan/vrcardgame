﻿using UnityEngine;
using System.Collections;
using SprocketTools.Base;

public class AIGlobalTarget : MonoBehaviour
{
    public int ID = 127;

    void Start()
    {
        KGameObjectSingleton<AIWorldBlackboard>.Instance.RegisterAITarget(gameObject, ID);
    }
}
