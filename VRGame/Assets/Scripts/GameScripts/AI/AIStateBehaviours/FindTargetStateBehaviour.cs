﻿using UnityEngine;
using System.Collections;

public class FindTargetStateBehaviour : AAIStateBehaviour
{
    public const string visibilityController_key = "visibilityController";
    public const string lastAITargetPosition_key = "lastAITargetPosition";

    public string targetFoundBoolAnimatorParamName = "TargetFound";
    public float timeToRememberLastPosition = 3f;

    private int targetFoundBoolParamId = -1;
    private float lastTimeRemeberUpdated;

    public GameObject currentAITarget = null;
    
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (targetFoundBoolParamId < 0)
            targetFoundBoolParamId = Animator.StringToHash(targetFoundBoolAnimatorParamName);
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (CanUpdateBehaviour())
        {
            if (currentAITarget != null)
            {
                aiBlackboard.Data[lastAITargetPosition_key] = (Vector3?)currentAITarget.transform.position;
                animator.SetBool(targetFoundBoolParamId, true);
                lastTimeRemeberUpdated = Time.timeSinceLevelLoad;
            }
            else
            {
                if (Time.timeSinceLevelLoad - lastTimeRemeberUpdated >= timeToRememberLastPosition)
                {
                    animator.SetBool(targetFoundBoolParamId, false);
                    aiBlackboard.Data[lastAITargetPosition_key] = null;
                }
            }
        }
    }
}
