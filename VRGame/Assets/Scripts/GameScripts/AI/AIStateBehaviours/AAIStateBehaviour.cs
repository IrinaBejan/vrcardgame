﻿using UnityEngine;
using System.Collections;

public abstract class AAIStateBehaviour : StateMachineBehaviour
{
    public float updateBehaviourDelaySeconds;
    private float lastBehaviourUpdateTime;

    protected bool allowUpdate;

    [System.NonSerialized]
    public AIBlackboard aiBlackboard;

    public bool CanUpdateBehaviour()
    {
        bool canUpdate = Time.timeSinceLevelLoad - lastBehaviourUpdateTime >= updateBehaviourDelaySeconds;
        if (canUpdate)
            lastBehaviourUpdateTime = Time.timeSinceLevelLoad;

        return canUpdate;
    }

    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) 
    //{
    //}

    //public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //}

    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) 
    //{
    //}

    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) 
    //{
    //}

    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) 
    //{
    //}
}