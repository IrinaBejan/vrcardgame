﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(LocText))]
public class LocTextEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        ((LocText)target).LocId = EditorGUILayout.TextField("Localisation Id", ((LocText)target).LocId);
        //SceneView.RepaintAll();
    }
}