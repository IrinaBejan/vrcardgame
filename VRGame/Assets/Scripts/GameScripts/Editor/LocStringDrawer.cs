﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(LocString))]
public class LocStringDrawer : PropertyDrawer
{
    private MethodInfo boldFontMethodInfo = null;

    private void SetBoldDefaultFont(bool value)
    {
        if (boldFontMethodInfo == null)
        {
            boldFontMethodInfo = typeof(EditorGUIUtility).GetMethod("SetBoldDefaultFont", BindingFlags.Static | BindingFlags.NonPublic);
        }

        boldFontMethodInfo.Invoke(null, new[] { value as object });
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var field = property.serializedObject.targetObject.GetType().GetField(property.name, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

        var locString = (LocString)field.GetValue(property.serializedObject.targetObject);

        bool isInstantiatedPrefab = property.isInstantiatedPrefab;
        if (isInstantiatedPrefab)
        {
            SetBoldDefaultFont(property.prefabOverride);
        }

        FieldInfo locIdField = typeof(LocString).GetField("locId", BindingFlags.NonPublic | BindingFlags.Instance);

        string currentValue = (string)locIdField.GetValue(locString);

        var targetId = EditorGUI.TextField(position, string.Format("[LocID] {0}", property.displayName), currentValue);

        if ((targetId != "") && (targetId != currentValue))
            //locIdField.SetValue(locString, LocString.Create(targetId));
            field.SetValue(property.serializedObject.targetObject, LocString.Create(targetId));

        if (GUI.changed)
            EditorUtility.SetDirty(property.serializedObject.targetObject);
    }
}
