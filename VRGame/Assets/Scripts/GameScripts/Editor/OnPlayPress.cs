﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

[InitializeOnLoad]
public class OnPlayPress
{

    static OnPlayPress()
    {

        EditorApplication.playmodeStateChanged = () =>
        {

            if (EditorApplication.isPlayingOrWillChangePlaymode && !EditorApplication.isPlaying)
            {

                Debug.Log("Auto-Saving scene before entering Play mode: " +EditorSceneManager.GetActiveScene().name);
                EditorSceneManager.SaveOpenScenes();
                EditorApplication.SaveAssets();
            }

        };

    }

}