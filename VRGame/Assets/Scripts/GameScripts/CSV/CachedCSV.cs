﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using SprocketTools.CrossPlatformShared;

public class CachedCSV
{
    private static readonly Regex csvSplitPattern = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
    private static readonly Regex headerTypePattern = new Regex("\\[\\S+\\]"); 

    private string[][] CSVCache = null;

    public Dictionary<string, int> fieldIndex = new Dictionary<string, int>();
    public Dictionary<string, Type> fieldTypes = new Dictionary<string, Type>();

    private string filePath = "";
    
    private DateTime fileTimestamp;

    private FileSystemWatcher fsw;

    private bool valid = false;

    public event EventHandler OnCacheRefreshed;

    public IEnumerable<string> Fields
    {
        get
        {
            if (!valid)
                yield break;

            foreach (var field in fieldIndex.Keys)
            {
                yield return field;
            }

            yield break;
        }
    }

    public IEnumerable<string[]> Entries
    {
        get
        {
            if (!valid)
                yield break;

            foreach (var entry in CSVCache)
            {
                if (entry == GetHeaderRow())
                    continue;

                yield return entry;
            }

            yield break;
        }
    }

    public bool IsValid
    {
        get { return valid; }
    }

    public string FilePath
    {
        get { return valid ? filePath : null; }

        private set
        {
            filePath = value;

            if (File.Exists(filePath) || filePath.Contains("://"))
            {
                valid = true;
            }
            else
            {
                valid = false;
            }

            if (!filePath.Contains("://"))
            {
                if (fsw == null)
                {
                    fsw = new FileSystemWatcher(Path.GetDirectoryName(filePath), Path.GetFileName(filePath));
                    fsw.EnableRaisingEvents = true;
                    fsw.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.Size;
                    fsw.Changed += fsw_Changed;
                    fsw.Renamed += fsw_Renamed;
                    fsw.Deleted += fsw_Deleted;
                }

                var timestamp = File.GetLastWriteTime(filePath);

                Refresh(timestamp);
            }
            else
            {
                Refresh(DateTime.Now);
            }
        }
    }
    
    public string this[int entryIndex, string fieldName]
    {
        get
        {
#if !DEBUG //bug here TODO: FIX
			if (!fieldIndex.ContainsKey(fieldName))
				return "";
#endif

            return CSVCache[entryIndex + 1][fieldIndex[fieldName]];
        }
    }

    public Type GetFieldType(string fieldName)
    {
#if !DEBUG //bug here TODO: FIX
		if (!fieldTypes.ContainsKey(fieldName))
			return null;
#endif

        return fieldTypes[fieldName];
    }

    private void RebuildFieldTypesAndIndices()
    {
        int index = 0;

        foreach (var field in GetHeaderRow())
        {
            if (headerTypePattern.IsMatch(field))
            {
                var typeName = headerTypePattern.Match(field).Value;
                var fieldType = Type.GetType(typeName.Substring(1, typeName.Length - 2)); 

                var fieldName = headerTypePattern.Replace(field, "").Trim();

                fieldIndex[fieldName] = index;

                fieldTypes[fieldName] = fieldType;
            }

            index++;
        }
    }

    private string[] GetHeaderRow()
    {
        //TODO: check for empty first row?
        return CSVCache[0];
    }

    private CachedCSV(string inputPath)
    {
        FilePath = inputPath;
    }
    
    void fsw_Deleted(object sender, FileSystemEventArgs e)
    {
        valid = false;
    }

    void fsw_Renamed(object sender, RenamedEventArgs e)
    {
        FilePath = e.Name;
    }

    void fsw_Changed(object sender, FileSystemEventArgs e)
    {
        RefreshIfOutdated();
    }

    private void Refresh(DateTime timestamp)
    {
        fileTimestamp = timestamp;

        CSVCache = ReadCSVFile(filePath);

        RebuildFieldTypesAndIndices();

        UnityEngine.Debug.Log(ToString());

        var eh = OnCacheRefreshed;

        if (eh != null)
        {
            eh(this, null);
        }
    }

    public void RefreshIfOutdated()
    {
        var timestamp = File.GetLastWriteTime(filePath);

        if (timestamp > fileTimestamp)
        {
            Refresh(timestamp);
        }
    }

    private string[] ReadCSVLine(string input)
    {
        var output = new List<string>();

        try
        {
            output.AddRange(csvSplitPattern.Split(input));
        }
        catch (System.Exception ex)
        {
            UnityEngine.Debug.LogException(ex);
        }

        return output.ToArray();
    }

    private string[][] ReadCSVFile(string inputPath)
    {
        var output = new List<string[]>();

        if (inputPath.Contains("://"))
        {
            try
            {
                using (WWW www = new WWW(inputPath))
                {
                    if (!string.IsNullOrEmpty(www.error))
                    {
                        Debug.LogErrorFormat("CachedCSV::ReadCSVFile - {0}", www.error);
                        return output.ToArray();
                    }

                    int timeoutCounter = 4000;
                    while (!www.isDone && (timeoutCounter > 0)) //blocking
                    {
                        ThreadHelper.Sleep(4);
                        timeoutCounter -= 4;
                    }

                    if (timeoutCounter <= 0)
                    {
                        Debug.LogWarningFormat("CachedCSV::ReadCSVFile - Trying to read CSV data timed out: '{0}'. Progress {1}", inputPath, www.progress);
                    }

                    using (TextReader reader = new StringReader(Encoding.UTF8.GetString(www.bytes, 0, www.bytes.Length)))
                    {
                        var currentLine = reader.ReadLine();

                        while (currentLine != null)
                        {
                            output.Add(ReadCSVLine(currentLine));

                            currentLine = reader.ReadLine();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                if (!(ex is FileNotFoundException || ex is DirectoryNotFoundException))
                {
                    Debug.LogWarningFormat("CachedCSV::ReadCSVFile - Trying to read CSV data raised an exception: {0}\n{1}", inputPath, ex.Message);
                }
                return output.ToArray();
            }
        }
        else
        {
            StreamReader filestream = null;

            try
            {
                using (var stream = new FileStream(inputPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    filestream = new StreamReader(stream);

                    var currentLine = filestream.ReadLine();

                    while (currentLine != null)
                    {
                        output.Add(ReadCSVLine(currentLine));

                        currentLine = filestream.ReadLine();
                    }

                    return output.ToArray();
                }
            }
            catch (Exception ex)
            {
                if (!(ex is FileNotFoundException || ex is DirectoryNotFoundException))
                {
                    Debug.LogWarningFormat("CachedCSV::ReadCSVFile - Trying to read CSV data raised an exception: {0}\n{1}", inputPath, ex.Message);
                }

                return output.ToArray();
            }
            finally
            {
                if (filestream != null)
                    FileAccessHelper.xpClose(ref filestream);
            }
        }

        return output.ToArray();
    }

    public override string ToString()
    {
        var outputString = "";

        if (CSVCache != null)
        {
            for (int i = 0; i < CSVCache.Length; ++i)
            {
                var line = CSVCache[i];

                if (i != 0)
                    outputString += "\n";

                for (int j = 0; j < line.Length; ++j)
                {
                    var field = line[j];

                    if (j != 0)
                        outputString += " | ";

                    outputString += field;
                }
            }
        }
        else
        {
            return "NULL";
        }

        return outputString;
    }

    public static CachedCSV FromFile(string inputPath)
    {
        var csvRet = new CachedCSV(inputPath);

        return csvRet;
    }

    public int EntryCount
    {
        get
        {
            return CSVCache.Length - 1;
        }
    }
}
