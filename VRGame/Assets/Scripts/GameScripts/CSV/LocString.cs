﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class LocString
{
    [SerializeField]
    private string locId;

    private string cache = "";

    private static Dictionary<string, LocString> registeredLocStrings = new Dictionary<string, LocString>();

    public static LocString Create(string locId)
    {
        if (!registeredLocStrings.ContainsKey(locId))
        {
            registeredLocStrings.Add(locId, new LocString(locId));
        }

        return registeredLocStrings[locId];
    }

    public void Refresh()
    {
        if (registeredLocStrings.ContainsKey(locId))
        {
            cache = registeredLocStrings[locId];
        }
    }
    
    public static void Reload()
    {
        foreach (var kvp in registeredLocStrings)
        {
            kvp.Value.Load();
        }
    }

    private void Load()
    {
        cache = LocManager.Get(locId);
    }

    protected LocString(string _locId)
    {
        locId = _locId;
    }

    public static implicit operator string(LocString source)
    {
        return source.cache;
    }
}
