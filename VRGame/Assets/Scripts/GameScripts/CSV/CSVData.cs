﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

public class CSVData<TObject> : IEnumerable<TObject> where TObject : ScriptableObject, new()
{
    public static PropertyInfo xpGetProperty(Type t, string name, BindingFlags flags)
    {
        return t.GetProperty(name, flags);
    }

    public static FieldInfo xpGetField(Type t, string name, BindingFlags flags)
    {
        return t.GetField(name, flags);
    }

    TObject[] _container;

    CachedCSV cache;

    public CSVData(string filePath)
    {
        cache = CachedCSV.FromFile(filePath);

        cache.OnCacheRefreshed += Refresh;

        Refresh(cache, null);
    }

    public TObject[] container
    { get { return _container; } }

    //never call on a live build outside load time
    //TODO: optimize baws
    private void Refresh(object sender, EventArgs e)
    {
        var tempContainer = new List<TObject>(cache.EntryCount);

        foreach (var entry in cache.Entries)
        {
            var obj = (TObject)ScriptableObject.CreateInstance<TObject>();

            foreach (var field in cache.Fields)
            {
                PropertyInfo prop = xpGetProperty(typeof(TObject), field, BindingFlags.Public | BindingFlags.Instance);

                var typedValue = ConstructTypedObject(cache.fieldTypes[field], entry[cache.fieldIndex[field]]);

                if (prop != null)
                {
                    prop.SetValue(obj, typedValue, null);
                }
                else
                {
                    FieldInfo fieldInfo = xpGetField(typeof(TObject), field, BindingFlags.Public | BindingFlags.Instance);

                    if (fieldInfo != null)
                    {
                        fieldInfo.SetValue(obj, typedValue);
                    }
                }
            }

            tempContainer.Add(obj);
        }

        _container = tempContainer.ToArray();
    }

    private object ConstructTypedObject(Type type, string value)
    {
        if (type == typeof(Type))
        {
            return Type.GetType(value);
        }
        else
        if (type == typeof(Int32))
        {
            return Int32.Parse(value);
        }
        else
        if (type == typeof(UInt32))
        {
            return UInt32.Parse(value);
        }
        else
        if (type == typeof(Single))
        {
            return Single.Parse(value);
        }
        else
        if (type == typeof(String))
        {
            return value.Replace("\"", "");
        }

        return null;
    }

    #region IEnumerable<TObject> Members

    public IEnumerator<TObject> GetEnumerator()
    {
        return ((IEnumerable<TObject>)container).GetEnumerator();
    }

    #endregion

    #region IEnumerable Members

    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
        return container.GetEnumerator();
    }

    #endregion
}
