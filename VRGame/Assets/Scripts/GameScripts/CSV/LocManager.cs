﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;

public static class LocManager
{
    private static CSVData<LocData> csvData;

    public enum Locale
    {
        Placeholder
    }

    private static Locale currentLocale = Locale.Placeholder;

    public static Locale CurrentLocale
    {
        get
        {
            return currentLocale;
        }

        set
        {
            currentLocale = value;
            Load();
        }
    }

    public static void Load()
    {
        csvData = new CSVData<LocData>(Application.streamingAssetsPath + "/LocText.csv");

        foreach (var locData in csvData)
        {
            LocString.Create(locData.Key);
        }

        LocString.Reload();
    }

    public static string Get(string key)
    {
        if (key == string.Empty)
            return string.Empty;

        var found = csvData.Where(l => l.Key == key);

        if (found.Count() == 0)
            return key;

        return found.First()[currentLocale];
    }
}
