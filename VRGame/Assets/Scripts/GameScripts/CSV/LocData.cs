﻿using UnityEngine;

public class LocData : ScriptableObject
{
	public readonly string Key;

	public readonly string Placeholder;

	public string this[LocManager.Locale locale]
	{
		get
		{
			switch (locale)
			{
				case LocManager.Locale.Placeholder: return Placeholder;
				default: return Key;
			}
		}
	}
}
