﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LocText : Text
{
    [SerializeField]
    [HideInInspector]
    protected string locId;

    protected override void Start()
    {
        base.Start();

        if (locId != null && locId.Length > 0)
            text = LocManager.Get(locId);
    }

    public string LocId
    {
        get { return locId; }
        set
        {
            if (locId != value)
            {
                locId = value;
                OnlyWhenChanged();
            }
        }
    }

    void OnlyWhenChanged()
    {
        m_Text = LocManager.Get(locId);
        Debug.Log(m_Text);
        UpdateGeometry();
    }
}
