﻿using UnityEngine;
using System.Collections;
using SprocketTools.Base;

public class TouchController : KMonoBehaviour
{
    private Vector3 previousPosition;
    private Vector3 nextPosition;
    private float xAngle = 0;
    private float yAngle = 0;
    private float previousXAngle = 0;
    private float previousYAngle = 0;

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            previousPosition = Input.mousePosition;
            previousXAngle = xAngle;
            previousYAngle = yAngle;
        }

        if (Input.GetMouseButton(0))
        {
            nextPosition = Input.mousePosition;
            xAngle = (previousXAngle - (nextPosition.x - previousPosition.x) * 180 / Screen.width);
            yAngle = (previousYAngle - (nextPosition.y - previousPosition.y) * 90 / Screen.height);
        }

        yAngle = Mathf.Clamp(yAngle, -90, 90);

        transform.eulerAngles = new Vector3(-yAngle, xAngle, 0);
#else
		if (Input.touchCount > 0)
		{
			if (Input.GetTouch(0).phase == TouchPhase.Began)
			{
				previousPosition = Input.GetTouch(0).position;
				previousXAngle = xAngle;
				previousYAngle = yAngle;
			}

			if (Input.GetTouch(0).phase == TouchPhase.Moved)
			{
				nextPosition = Input.GetTouch(0).position;
				xAngle = (previousXAngle - (nextPosition.x - previousPosition.x) * 180 / Screen.width);
				yAngle = (previousYAngle - (nextPosition.y - previousPosition.y) * 90 / Screen.height);
			}

			yAngle = Mathf.Clamp(yAngle, -90, 90);

			transform.eulerAngles = new Vector3(-yAngle, xAngle, 0);
		}
#endif
    }
}
