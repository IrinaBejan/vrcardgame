﻿using UnityEngine;


[DisallowMultipleComponent]
[RequireComponent(typeof(AbLocomotion))]
[RequireComponent(typeof(CharacterPhysicsController))]
public class CharacterInputController : MonoBehaviour
{
    public string animatorAttackTriggerName = "Attack";

    public AbLocomotion locomotionController = null;
    public CharacterPhysicsController characterPhysicsController = null;

    public GameObject inputProviderGameObj = null;
    public ICharacterInputProvider inputProvider = null;
    public Camera characterCamera = null;

    public enum EInputDirectionalMode
    {
        WorldRelative,
        CameraRelative,
        CharacterRelative,
    }

    public EInputDirectionalMode inputDirectionalMode;

    #region MonoBehaviour
    public void Awake()
    {
        gameObject.CheckAndInitializeWithInterface(ref locomotionController);
        gameObject.CheckAndInitializeWithInterface(ref characterPhysicsController, true);

        if (inputProviderGameObj == null)
            inputProviderGameObj = gameObject;

        inputProviderGameObj.CheckAndInitializeWithInterface(ref inputProvider);

        inputProvider.OnJump += HandleOnJumpInput;
        inputProvider.OnLightAttack += HandleOnLightAttack;
    }

    public void Update()
    {
        Vector3 inputVelocity = inputProvider.GetCharacterInputVelocity();

        switch (inputDirectionalMode)
        {
            case EInputDirectionalMode.CharacterRelative:
                locomotionController.TargetVelocity = transform.TransformDirection(inputVelocity);
                break;
            case EInputDirectionalMode.WorldRelative:
                locomotionController.TargetVelocity = inputVelocity;
                break;
            default:
                break;
        }
    }

    void HandleOnJumpInput(object sender, InputEventArgs args)
    {
        if (characterPhysicsController != null)
            characterPhysicsController.Jump(1f);
    }

    void HandleOnLightAttack(object sender, InputEventArgs args)
    {
        if (characterPhysicsController != null)
            characterPhysicsController.animator.SetTrigger(animatorAttackTriggerName);
    }
    #endregion //MonoBehaviour
}
